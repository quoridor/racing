# How to build

1. Install wasm-pack:
```
https://rustwasm.github.io/wasm-pack/installer/
```

2. `cd racing`

3. `npm install`

4. `npm run serve`

# TODO:
- physics: more accurate braking
- physics: weight transfer
- physics: torque curve for engine power
- physics: gear ratios
- physics: slip ratio

# Useful links
[Car physics](https://asawicki.info/Mirror/Car%20Physics%20for%20Games/Car%20Physics%20for%20Games.html)