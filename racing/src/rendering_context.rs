use std::collections::HashMap;
use std::f64::consts::PI;
use std::rc::Rc;
use web_sys::{CanvasRenderingContext2d, HtmlImageElement};
use wasm_bindgen::JsCast;
use crate::color::Color;
use crate::components::camera::CameraComponent;
use crate::components::position::PositionComponent;
use crate::geometry::{Angle, Dimensions, Point};
use crate::console_log;
use crate::utils::performance_now;
use crate::world::World;

pub struct RenderingContext {
    context: CanvasRenderingContext2d,
    dimensions: Dimensions,

    loaded_images: HashMap<String, Rc<HtmlImageElement>>,
    prev_frame_time: f64,

    camera_position: Point,
}

#[derive(Clone)]
pub struct Image {
    source: String,
    offset: Point,
    dimensions: Dimensions,
}

impl RenderingContext {

    pub fn new() -> Self {
        let window = web_sys::window().unwrap();
        let document = window.document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .unwrap();

        canvas.set_width(window.inner_width().unwrap().as_f64().unwrap() as u32);
        canvas.set_height(window.inner_height().unwrap().as_f64().unwrap() as u32);

        Self {
            context: canvas
                .get_context("2d")
                .unwrap()
                .unwrap()
                .dyn_into::<web_sys::CanvasRenderingContext2d>()
                .unwrap(),
            dimensions: Dimensions::new(canvas.width() as f64, canvas.height() as f64),

            loaded_images: HashMap::new(),
            prev_frame_time: performance_now(),

            camera_position: Point::zero(),
        }
    }

    pub fn compute_delta(&mut self, now: f64) -> f64 {
        let delta = (now - self.prev_frame_time).max(0.0);
        self.prev_frame_time = now;
        delta
    }

    pub fn on_new_frame(&mut self, world: &World) {
        self.camera_position = world.objects_by_component::<CameraComponent>()
            .map(|v| v.component::<PositionComponent>().unwrap().point().clone())
            .next()
            .unwrap();

        self.clear();
    }

    pub fn clear(&self) {
        self.context.clear_rect(0.0, 0.0, self.dimensions.width(), self.dimensions.height());
    }

    pub fn draw_image(&mut self, image: &Image, position: &Point, size: &Dimensions) {
        let img = self.resolve_image(&image.source);

        let screen_x = self.to_screen_x(position.x());
        let screen_y = self.to_screen_y(position.y());

        if screen_x + size.width() < 0.0 {
            return;
        } else if screen_y + size.height() < 0.0 {
            return;
        } else if screen_x > self.dimensions.width() {
            return;
        } else if screen_y > self.dimensions.height() {
            return;
        }

        self.context.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
            &img,
            image.offset.x(),
            image.offset.y(),
            image.dimensions.width(),
            image.dimensions.height(),
            screen_x,
            screen_y,
            size.width(),
            size.height(),
        ).unwrap();
    }

    pub fn draw_image_with_rotation(&mut self, image: &Image, position: &Point, size: &Dimensions, angle: &Angle) {
        let angle_radians = angle.radians();
        let x = self.to_screen_x(position.x());
        let y = self.to_screen_y(position.y());

        self.context.save();
        self.context.translate(size.width() / 2.0 + x, size.height() / 2.0 + y);
        self.context.rotate(angle_radians);
        self.context.translate(-size.width() / 2.0, -size.height() / 2.0);

        let img = self.resolve_image(&image.source);
        self.context.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
            &img,
            image.offset.x(),
            image.offset.y(),
            image.dimensions.width(),
            image.dimensions.height(),
            0.0,
            0.0,
            size.width(),
            size.height(),
        ).unwrap();

        self.context.restore();
    }

    pub fn draw_debug_point(&mut self, point: &Point) {
        let point = self.to_screen(point);

        self.context.begin_path();
        self.context.arc(point.x(), point.y(), 5.0, 0.0, 2.0 * PI).unwrap();
        self.context.stroke();
    }

    pub fn draw_debug_line(&mut self, from: &Point, to: &Point) {
        let from = self.to_screen(from);
        let to = self.to_screen(to);

        self.context.begin_path();
        self.context.move_to(from.x(), from.y());
        self.context.line_to(to.x(), to.y());
        self.context.stroke();
    }

    pub fn draw_debug_line_with_color(&mut self, from: &Point, to: &Point, color: &Color) {
        let stroke_style = self.context.stroke_style();
        self.context.set_stroke_style(&color.to_css().into());
        self.draw_debug_line(from, to);
        self.context.set_stroke_style(&stroke_style);
    }

    pub fn draw_debug_text(&mut self, point: &Point, text: &str) {
        self.context.fill_text(text, point.x(), point.y());
    }

    pub fn draw_prompt_text(&mut self, text: &str) {
        let font = self.context.font();
        self.context.set_font("40px Open Sans");
        self.context.fill_text(text, (self.dimensions.width() - (text.len() as f64 * 16.0)) / 2.0, self.dimensions.height() / 4.0);
        self.context.set_font(&font);
    }

    pub fn draw_text(&mut self, point: &Point, text: &str) {
        let font = self.context.font();
        self.context.set_font("40px Open Sans");
        self.context.fill_text(text, point.x(), point.y());
        self.context.set_font(&font);
    }

    fn resolve_image(&mut self, source: &str) -> Rc<HtmlImageElement> {
        if !self.loaded_images.contains_key(source) {
            console_log(&format!("loading image: {}", source));
            let img = web_sys::window().unwrap()
                .document().unwrap()
                .create_element("img").unwrap()
                .dyn_into::<web_sys::HtmlImageElement>()
                .unwrap();
            img.set_src(&source);
            self.loaded_images.insert(source.to_owned(), Rc::new(img));
        }

        return self.loaded_images.get(source).unwrap().clone();
    }

    fn to_screen(&self, position: &Point) -> Point {
        Point::new(
            self.to_screen_x(position.x()),
            self.to_screen_y(position.y()),
        )
    }

    fn to_screen_x(&self, x: f64) -> f64 {
        x - self.camera_position.x() + (&self.dimensions.width() / 2.0)
    }

    fn to_screen_y(&self, y: f64) -> f64 {
        y - self.camera_position.y() + (&self.dimensions.height() / 2.0)
    }

    pub fn camera_position(&self) -> &Point {
        &self.camera_position
    }

    pub fn dimensions(&self) -> &Dimensions {
        &self.dimensions
    }
}

impl Image {
    pub const fn new(source: String, offset: Point, dimensions: Dimensions) -> Self {
        Self {
            source,
            offset,
            dimensions,
        }
    }
}