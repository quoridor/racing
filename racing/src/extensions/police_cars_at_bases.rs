use crate::components::car::{CarComponent, CarType};
use crate::components::desired_velocity::DesiredVelocityComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::max_engine_power::MaxEnginePowerComponent;
use crate::components::police_base::PoliceBaseComponent;
use crate::components::police_car::PoliceCarComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::extensions::car_physics::WithCarPhysics;
use crate::geometry::{Dimensions, Point};
use crate::world::{World, WorldObject};

pub trait WithPoliceCarsAtPoliceBases {
    fn with_police_car_at_each_police_base(self) -> Self;
}

impl WithPoliceCarsAtPoliceBases for World {
    fn with_police_car_at_each_police_base(mut self) -> Self {
        let base_positions: Vec<Point> = self.objects_by_component::<PoliceBaseComponent>()
            .map(|v| v.component::<PositionComponent>().unwrap().point().clone())
            .collect();

        for i in 0..base_positions.len() {
            self = self.with_object(WorldObject::new()
                .with_component(PoliceCarComponent::new())
                .with_component(IdComponent::new(Id::new(format!("police-car-{}", i))))
                .with_component(CarComponent::new(CarType::PoliceCar))
                .with_component(PositionComponent::new(base_positions[i].clone()))
                .with_component(DimensionsComponent::new(Dimensions::new(71.0, 131.0)))
                .with_component(RotationComponent::new(90.0))
                .with_component(MaxEnginePowerComponent::new(0.8))
                .with_component(DesiredVelocityComponent::new())
                .with_car_physics())
        }

        self
    }
}