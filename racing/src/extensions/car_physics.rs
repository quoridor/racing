use crate::components::collision_body::CollisionBodyComponent;
use crate::components::collision_force::CollisionForceComponent;
use crate::components::drag_force::DragForceComponent;
use crate::components::engine_force::EngineForceComponent;
use crate::components::latitudinal_force::LatitudinalForceComponent;
use crate::components::longtitudinal_force::LongtitudinalForceComponent;
use crate::components::rolling_resistance::RollingResistanceComponent;
use crate::components::steering_angle::SteeringAngleComponent;
use crate::components::traction_force::TractionForceComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{Dimensions, Polygon, Vector};
use crate::world::WorldObject;

pub trait WithCarPhysics {
    fn with_car_physics(self) -> Self;
}

impl WithCarPhysics for WorldObject {
    fn with_car_physics(self) -> Self {
        self.with_component(EngineForceComponent::new())
            .with_component(TractionForceComponent::new())
            .with_component(VelocityComponent::new(Vector::new(0.0, 0.0)))
            .with_component(DragForceComponent::new())
            .with_component(RollingResistanceComponent::new())
            .with_component(LongtitudinalForceComponent::new())
            .with_component(SteeringAngleComponent::new())
            .with_component(LatitudinalForceComponent::new())
            .with_component(CollisionBodyComponent::new(Polygon::rectangle(Dimensions::new(71.0, 131.0))))
            .with_component(CollisionForceComponent::new())
    }
}