use rand::prelude::IteratorRandom;
use crate::components::collision_body::CollisionBodyComponent;
use crate::components::collision_force::CollisionForceComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::moving_to_object::MovingToObjectComponent;
use crate::components::moving_to_position::MovingToPositionComponent;
use crate::components::pedestrian::{PedestrianComponent, PedestrianType};
use crate::components::pedestrian_movement_resistance::PedestrianMovementResistanceComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::components::velocity::VelocityComponent;
use crate::components::walking_force::WalkingForce;
use crate::geometry::{Dimensions, Point, Polygon, Vector};
use crate::utils::console_log;
use crate::world::{World, WorldObject};

pub trait WithPedestriansEverywhere {
    fn with_pedestrians_everywhere(self) -> Self;
}

impl WithPedestriansEverywhere for World {

    fn with_pedestrians_everywhere(self) -> Self {
        let mut world = self;

        let mut existing_positions = Vec::new();
        for pos in world.objects_by_component::<PositionComponent>() {
            existing_positions.push(pos.component::<PositionComponent>().unwrap().point().clone());
        }

        for i in 0..8 {
            let x = (-100..8000).choose(&mut rand::thread_rng()).unwrap() as f64;
            let y = (-100..8000).choose(&mut rand::thread_rng()).unwrap() as f64;
            let point = Point::new(x, y);

            let mut too_close = false;
            for pos in &existing_positions {
                if pos.distance_to_point(&point) <= 500.0 {
                    too_close = true;
                }
            }

            if too_close {
                continue;
            }
            existing_positions.push(point.clone());

            let target_x = (-100..8000).choose(&mut rand::thread_rng()).unwrap() as f64;
            let target_y = (-100..8000).choose(&mut rand::thread_rng()).unwrap() as f64;

            let leader_id = Id::new(format!("pedestrian-{}", i));
            world = world.with_object(WorldObject::new()
                .with_component(PedestrianComponent::new(PedestrianType::PedestrianBlueBlond))
                .with_component(IdComponent::new(leader_id.clone()))
                .with_component(PositionComponent::new(point.clone()))
                .with_component(DimensionsComponent::new(Dimensions::new(75.0, 71.0)))
                .with_component(RotationComponent::new(0.0))
                .with_component(VelocityComponent::zero())
                .with_component(CollisionBodyComponent::new(Polygon::rectangle(Dimensions::new(75.0, 71.0))))
                .with_component(CollisionForceComponent::new())
                .with_component(PedestrianMovementResistanceComponent::new())
                .with_component(WalkingForce::new())
                .with_component(MovingToPositionComponent::new(Point::new(target_x, target_y))));

            let group_size = (0..5).choose(&mut rand::thread_rng()).unwrap();
            for n in 0..group_size {
                let group_member_point = &point + Vector::new(200.0 * n as f64, 0.0);

                world = world.with_object(WorldObject::new()
                    .with_component(PedestrianComponent::new(PedestrianType::PedestrianBlueBlond))
                    .with_component(IdComponent::new(Id::new(format!("pedestrian-{}-{}", i, n))))
                    .with_component(PositionComponent::new(group_member_point))
                    .with_component(DimensionsComponent::new(Dimensions::new(75.0, 71.0)))
                    .with_component(RotationComponent::new(0.0))
                    .with_component(VelocityComponent::zero())
                    .with_component(CollisionBodyComponent::new(Polygon::rectangle(Dimensions::new(75.0, 71.0))))
                    .with_component(CollisionForceComponent::new())
                    .with_component(PedestrianMovementResistanceComponent::new())
                    .with_component(WalkingForce::new())
                    .with_component(MovingToObjectComponent::new(leader_id.clone())));
            }
        }

        world
    }
}