use rand::prelude::*;
use rand::Rng;
use crate::components::ai_car::AiCarComponent;
use crate::components::car::{CarComponent, CarType};
use crate::components::controllable_car::ControllableCarComponent;
use crate::components::desired_velocity::DesiredVelocityComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::lap_counter::LapCounterComponent;
use crate::components::max_engine_power::MaxEnginePowerComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::extensions::car_physics::WithCarPhysics;
use crate::geometry::{Dimensions, Point};
use crate::world::{World, WorldObject};

pub trait WithCars {
    fn with_cars(self, player_car_id: Id) -> Self;
}

impl WithCars for World {
    fn with_cars(self, player_car_id: Id) -> Self {
        let mut world = self;
        let player_index = (0..4).choose(&mut rand::thread_rng()).unwrap();

        for i in 0..10 {
            let car_color = vec![
                CarType::RedCar,
                CarType::GreenCar,
                CarType::BlueCar,
                CarType::YellowCar,
                CarType::BlackCar,
            ];
            let car_color = car_color.choose(&mut rand::thread_rng()).unwrap();

            let pos = if i % 2 == 0 {
                Point::new(4000.0 - i as f64 * 300.0, 5950.0)
            } else {
                Point::new(4000.0 - i as f64 * 300.0, 6150.0)
            };

            if i == player_index {
                world = world.with_object(WorldObject::new()
                    .with_component(ControllableCarComponent::new())
                    .with_component(IdComponent::new(player_car_id.clone()))
                    .with_component(CarComponent::new(car_color.clone()))
                    .with_component(PositionComponent::new(pos))
                    .with_component(DimensionsComponent::new(Dimensions::new(71.0, 131.0)))
                    .with_component(RotationComponent::new(90.0))
                    .with_component(MaxEnginePowerComponent::new(1.0))
                    .with_component(LapCounterComponent::new())
                    .with_car_physics())
            } else {
                world = world.with_object(WorldObject::new()
                    .with_component(CarComponent::new(car_color.clone()))
                    .with_component(IdComponent::new(Id::new(format!("car-{}", i))))
                    .with_component(PositionComponent::new(pos))
                    .with_component(DimensionsComponent::new(Dimensions::new(71.0, 131.0)))
                    .with_component(RotationComponent::new(90.0))
                    .with_component(MaxEnginePowerComponent::new(rand::thread_rng().gen_range(0.5..1.2)))
                    .with_component(DesiredVelocityComponent::new())
                    .with_component(AiCarComponent::new())
                    .with_car_physics())
            }
        }

        world
    }
}