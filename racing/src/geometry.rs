use std::cmp::Ordering;
use std::f64::consts::PI;
use std::ops::{Add, AddAssign, Div, Mul, Sub, SubAssign};

pub const DELTA: f64 = 0.00001;

#[derive(Clone, Debug)]
pub struct Point {
    x: f64,
    y: f64,
}

#[derive(Clone, Debug)]
pub struct Dimensions {
    width: f64,
    height: f64,
}

#[derive(Clone, Debug)]
pub struct Vector {
    x: f64,
    y: f64,
}

#[derive(Clone, Debug)]
pub struct Angle {
    degrees: f64,
}

#[derive(Clone, Debug)]
pub struct Polygon {
    points: Vec<Point>,
}

#[derive(Clone, Debug)]
pub struct Segment {
    points: (Point, Point),
}

impl Point {
    pub const fn new(x: f64, y: f64) -> Self {
        Self {
            x,
            y,
        }
    }

    pub fn zero() -> Self {
        Self::new(0.0, 0.0)
    }

    pub fn rotate_around_by(&self, around: &Point, by: &Angle) -> Point {
        let s = by.radians().sin();
        let c = by.radians().cos();

        let p = self - around;
        let rotated = Point::new(p.x() * c - p.y() * s, p.x() * s + p.y() * c);

        rotated + around
    }

    pub fn x(&self) -> f64 {
        self.x
    }

    pub fn y(&self) -> f64 {
        self.y
    }

    pub fn to_vector(&self) -> Vector {
        Vector::new(self.x(), self.y())
    }

    pub fn distance_to_point(&self, other: &Point) -> f64 {
        (other - self).to_vector().magnitude()
    }

    pub fn distance_to_segment(&self, segment: &Segment) -> (f64, Point) {
        let dx = segment.points.1.x() - segment.points.0.x();
        let dy = segment.points.1.y() - segment.points.0.y();

        if dx.abs() < DELTA && dy.abs() < DELTA {
            // it is a point, not a line segment
            let closest = segment.points.0.clone();
            let dx = self.x() - segment.points.0.x();
            let dy = self.y() - segment.points.0.y();
            return ((dx * dx + dy * dy).sqrt(), closest)
        }

        // Calculate the t part that minimizes the distance.
        let pt = self;
        let pt1 = &segment.points.0;
        let pt2 = &segment.points.1;
        let t = ((pt.x() - pt1.x()) * dx + (pt.y() - pt1.y()) * dy) / (dx * dx + dy * dy);

        // See if this represents one of the segment's
        // end points of a point in the middle
        let (closest, dx, dy) = if t < 0.0 {
            (pt1.clone(), pt.x() - pt1.x(), pt.y() - pt1.y())
        } else if t > 1.0 {
            (pt2.clone(), pt.x() - pt2.x(), pt.y() - pt2.y())
        } else {
            let closest = Point::new(pt1.x() + t * dx, pt1.y() + t * dy);
            (
                closest.clone(),
                pt.x() - closest.x(),
                pt.y() - closest.y(),
            )
        };

        ((dx * dx + dy * dy).sqrt(), closest)
    }
}

impl Dimensions {
    pub const fn new(width: f64, height: f64) -> Self {
        Self {
            width,
            height,
        }
    }

    pub fn to_vector(&self) -> Vector {
        Vector::new(self.width, self.height)
    }

    pub fn width(&self) -> f64 {
        self.width
    }

    pub fn height(&self) -> f64 {
        self.height
    }
}

impl Vector {
    pub fn new(x: f64, y: f64) -> Self {
        Self {
            x,
            y,
        }
    }

    pub fn from_points(pt1: &Point, pt2: &Point) -> Self {
        (pt2 - pt1).to_vector()
    }

    pub fn zero() -> Self {
        Self::new(0.0, 0.0)
    }

    pub fn x(&self) -> f64 {
        self.x
    }

    pub fn y(&self) -> f64 {
        self.y
    }

    pub fn magnitude(&self) -> f64 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn normalize(&self) -> Self {
        let length = self.magnitude();
        Vector::new(self.x / length, self.y / length)
    }

    pub fn angle(&self, other: &Vector) -> Angle {
        Angle::from_radians((self.dot(other) / (self.magnitude() * other.magnitude())).acos())
    }

    pub fn dot(&self, other: &Vector) -> f64 {
        self.x * other.x + self.y * other.y
    }

    pub fn is_nan(&self) -> bool {
        self.x().is_nan() || self.y().is_nan()
    }
}

impl Angle {
    pub fn from_degrees(degrees: f64) -> Self {
        Self {
            degrees,
        }
    }

    pub fn from_radians(radians: f64) -> Self {
        Self {
            degrees: radians * 180.0 / PI,
        }
    }

    pub fn zero() -> Self {
        Self::from_degrees(0.0)
    }

    pub fn degrees(&self) -> f64 {
        self.degrees
    }

    pub fn radians(&self) -> f64 {
        self.degrees * PI / 180.0
    }

    pub fn signum(&self) -> f64 {
        self.degrees.signum()
    }

    pub fn is_close_to_zero(&self) -> bool {
        (0.0 - self.degrees).abs() < DELTA
    }

    pub fn unit_vector(&self) -> Vector {
        let angle_radians = self.radians();
        Vector::new(angle_radians.sin(), -angle_radians.cos()).normalize()
    }
}

impl Polygon {
    pub fn new(points: Vec<Point>) -> Self {
        Self {
            points,
        }
    }

    pub fn rectangle(dimensions: Dimensions) -> Self {
        Self::new(vec![
            Point::new(0.0, 0.0),
            Point::new(dimensions.width(), 0.0),
            Point::new(dimensions.width(), dimensions.height()),
            Point::new(0.0, dimensions.height()),
        ])
    }

    pub fn points(&self) -> &Vec<Point> {
        &self.points
    }

    pub fn edges(&self) -> Vec<Segment> {
        let mut segments = Vec::new();
        for i in 0..self.points().len() {
            let pt0 = if i == 0 {
                self.points()[self.points().len() - 1].clone()
            } else {
                self.points()[i - 1].clone()
            };
            let pt1 = self.points[i].clone();
            segments.push(Segment::new((pt0, pt1)))
        }
        segments
    }

    pub fn center(&self) -> Point {
        let mut center = Point::new(0.0, 0.0);
        for point in self.points() {
            center += point;
        }
        Point::new(center.x() / self.points().len() as f64, center.y() / self.points().len() as f64)
    }

    pub fn translated_by(&self, translate_by: &Vector) -> Self {
        let mut translated_points = Vec::new();

        for point in self.points() {
            translated_points.push(point + translate_by);
        }

        Self::new(translated_points)
    }

    pub fn rotated_by(&self, angle: &Angle) -> Self {
        let center = self.center();
        let mut rotated_points = Vec::new();
        for point in self.points() {
            rotated_points.push(point.rotate_around_by(&center, angle));
        }
        Self::new(rotated_points)
    }

    pub fn contains(&self, point: &Point) -> bool {
        let mut c = false;

        let mut j = self.points().len()-1;
        for i in 0..self.points().len() {
            if (self.points()[i].y() >= point.y()) != (self.points()[j].y() >= point.y()) &&
                (point.x() <= (self.points()[j].x() - self.points()[i].x()) * (point.y() - self.points()[i].y()) / (self.points()[j].y() - self.points()[i].y()) + self.points[i].x()) {
                c = !c;
            }
            j = i;
        }

        c
    }

    pub fn intersects(&self, other: &Polygon) -> Option<&Point> {
        for point in self.points() {
            if other.contains(point) {
                return Some(point);
            }
        }

        None
    }

    pub fn intersects_segment(&self, segment: &Segment) -> bool {
        for edge in self.edges() {
            if edge.intersects_segment(segment) {
                return true;
            }
        }

        false
    }

    pub fn closest_edge(&self, point: &Point) -> (Segment, f64, Point) {
        let mut closest_edge = None;
        let mut closest_distance = 0.0;
        let mut closest_point = None;

        for segment in self.edges() {
            let (distance, segment_point) = point.distance_to_segment(&segment);
            if closest_edge.is_none() || distance < closest_distance {
                closest_distance = distance;
                closest_edge = Some(segment);
                closest_point = Some(segment_point);
            }
        }

        (closest_edge.unwrap(), closest_distance, closest_point.unwrap())
    }
}

impl Segment {
    pub fn new(points: (Point, Point)) -> Self {
        Self {
            points,
        }
    }

    // see https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
    pub fn intersects_segment(&self, other: &Segment) -> bool {
        let a = &self.points.0;
        let b = &self.points.1;
        let c = &other.points.0;
        let d = &other.points.1;

        Self::ccw(a, c, d) != Self::ccw(b, c, d) && Self::ccw(a, b, c) != Self::ccw(a, b, d)
    }

    fn ccw(a: &Point, b: &Point, c: &Point) -> bool {
        (c.y() - a.y()) * (b.x() - a.x()) > (b.y() - a.y()) * (c.x() - a.x())
    }
}

impl Add<&Point> for &Point {
    type Output = Point;

    fn add(self, rhs: &Point) -> Self::Output {
        Self::Output::new(self.x() + rhs.x(), self.y() + rhs.y())
    }
}

impl Add<&Point> for Point {
    type Output = Point;

    fn add(self, rhs: &Point) -> Self::Output {
        Self::Output::new(self.x() + rhs.x(), self.y() + rhs.y())
    }
}

impl Sub<&Point> for Point {
    type Output = Point;

    fn sub(self, rhs: &Point) -> Self::Output {
        Self::Output::new(self.x() - rhs.x(), self.y() - rhs.y())
    }
}

impl Sub<&Point> for &Point {
    type Output = Point;

    fn sub(self, rhs: &Point) -> Self::Output {
        Self::Output::new(self.x() - rhs.x(), self.y() - rhs.y())
    }
}


impl Add<Vector> for Point {
    type Output = Point;

    fn add(self, rhs: Vector) -> Self::Output {
        Point::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Add<Vector> for &Point {
    type Output = Point;

    fn add(self, rhs: Vector) -> Self::Output {
        Point::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Add<&Vector> for &Point {
    type Output = Point;

    fn add(self, rhs: &Vector) -> Self::Output {
        Point::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl AddAssign<Vector> for Point {
    fn add_assign(&mut self, rhs: Vector) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl AddAssign<&Vector> for Point {
    fn add_assign(&mut self, rhs: &Vector) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl AddAssign<&Point> for Point {
    fn add_assign(&mut self, rhs: &Point) {
        self.x += rhs.x();
        self.y += rhs.y();
    }
}

impl Div<f64> for &Dimensions {
    type Output = Dimensions;

    fn div(self, rhs: f64) -> Self::Output {
        Dimensions::new(self.width / rhs, self.height / rhs)
    }
}

impl Mul<f64> for Vector {
    type Output = Vector;

    fn mul(self, rhs: f64) -> Self::Output {
        Vector::new(self.x * rhs, self.y * rhs)
    }
}

impl Mul<f64> for &Vector {
    type Output = Vector;

    fn mul(self, rhs: f64) -> Self::Output {
        Vector::new(self.x * rhs, self.y * rhs)
    }
}

impl Div<f64> for Vector {
    type Output = Vector;

    fn div(self, rhs: f64) -> Self::Output {
        Vector::new(self.x / rhs, self.y / rhs)
    }
}

impl Div<f64> for &Vector {
    type Output = Vector;

    fn div(self, rhs: f64) -> Self::Output {
        Vector::new(self.x / rhs, self.y / rhs)
    }
}

impl Add<Vector> for Vector {
    type Output = Vector;

    fn add(self, rhs: Vector) -> Self::Output {
        Vector::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Add<&Vector> for &Vector {
    type Output = Vector;

    fn add(self, rhs: &Vector) -> Self::Output {
        Vector::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Sub<&Vector> for &Vector {
    type Output = Vector;

    fn sub(self, rhs: &Vector) -> Self::Output {
        Vector::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl Sub<Vector> for Vector {
    type Output = Vector;

    fn sub(self, rhs: Vector) -> Self::Output {
        Vector::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl AddAssign<Vector> for Vector {
    fn add_assign(&mut self, rhs: Vector) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl AddAssign<&Vector> for Vector {
    fn add_assign(&mut self, rhs: &Vector) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl SubAssign<Vector> for Vector {
    fn sub_assign(&mut self, rhs: Vector) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}


impl Mul<Vector> for f64 {
    type Output = Vector;

    fn mul(self, rhs: Vector) -> Self::Output {
        rhs * self
    }
}

impl Mul<&Vector> for f64 {
    type Output = Vector;

    fn mul(self, rhs: &Vector) -> Self::Output {
        rhs * self
    }
}

impl AddAssign<Angle> for Angle {
    fn add_assign(&mut self, rhs: Angle) {
        self.degrees += rhs.degrees;
    }
}

impl AddAssign<&Angle> for Angle {
    fn add_assign(&mut self, rhs: &Angle) {
        self.degrees += rhs.degrees;
    }
}

impl SubAssign<Angle> for Angle {
    fn sub_assign(&mut self, rhs: Angle) {
        self.degrees -= rhs.degrees;
    }
}

impl SubAssign<&Angle> for Angle {
    fn sub_assign(&mut self, rhs: &Angle) {
        self.degrees -= rhs.degrees;
    }
}

impl Mul<f64> for Angle {
    type Output = Angle;

    fn mul(self, rhs: f64) -> Self::Output {
        Self::from_degrees(self.degrees * rhs)
    }
}

impl Add<Angle> for Angle {
    type Output = Angle;

    fn add(self, rhs: Angle) -> Self::Output {
        Self::Output::from_degrees(self.degrees + rhs.degrees)
    }
}

impl Add<&Angle> for Angle {
    type Output = Angle;

    fn add(self, rhs: &Angle) -> Self::Output {
        Self::Output::from_degrees(self.degrees + rhs.degrees)
    }
}

impl Add<&Angle> for &Angle {
    type Output = Angle;

    fn add(self, rhs: &Angle) -> Self::Output {
        Self::Output::from_degrees(self.degrees + rhs.degrees)
    }
}

impl Add<Angle> for &Angle {
    type Output = Angle;

    fn add(self, rhs: Angle) -> Self::Output {
        Self::Output::from_degrees(self.degrees + rhs.degrees)
    }
}

impl PartialOrd<Self> for Angle {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.degrees.partial_cmp(&other.degrees)
    }
}

impl Ord for Angle {
    fn cmp(&self, other: &Self) -> Ordering {
        self.degrees.partial_cmp(&other.degrees).unwrap_or(Ordering::Equal)
    }
}

impl PartialEq<Self> for Angle {
    fn eq(&self, other: &Self) -> bool {
        (self.degrees - other.degrees).abs() < DELTA
    }
}

impl Eq for Angle {
}