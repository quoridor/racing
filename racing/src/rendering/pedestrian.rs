use crate::components::dimensions::DimensionsComponent;
use crate::components::pedestrian::PedestrianComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_pedestrians(world: &World, rendering_context: &mut RenderingContext) {
    for pedestrian in world.objects_by_component::<PedestrianComponent>() {
        let position = pedestrian.component::<PositionComponent>().unwrap();
        let dimensions = pedestrian.component::<DimensionsComponent>().unwrap();
        let rotation = pedestrian.component::<RotationComponent>().unwrap();

        let pedestrian = pedestrian.component::<PedestrianComponent>().unwrap();
        rendering_context.draw_image_with_rotation(
            pedestrian.image(), position.point(),
            dimensions.dimensions(), rotation.angle());
    }
}