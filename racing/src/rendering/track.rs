use crate::components::tile::TileComponent;
use crate::components::track::TrackComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_track(world: &World, rendering_context: &mut RenderingContext) {
    for track in world.objects_by_component::<TrackComponent>() {
        let track = track.component::<TrackComponent>().unwrap();
        for tile in track.tiles() {
            rendering_context.draw_image(tile.image(), tile.position(), tile.size());
        }
    }
}