use crate::components::lap_counter::LapCounterComponent;
use crate::geometry::Point;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_lap_counter(world: &World, rendering_context: &mut RenderingContext) {
    let lap_counter = world.objects_by_component::<LapCounterComponent>()
        .next().unwrap()
        .component::<LapCounterComponent>()
        .unwrap()
        .total_laps();

    rendering_context.draw_text(&Point::new(10.0, 40.0), &format!("Laps: {}", lap_counter));
}