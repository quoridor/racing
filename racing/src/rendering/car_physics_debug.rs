use crate::color::Color;
use crate::components::collision_body::CollisionBodyComponent;
use crate::components::collision_force::CollisionForceComponent;
use crate::components::controllable_car::ControllableCarComponent;
use crate::components::desired_velocity::DesiredVelocityComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::latitudinal_force::LatitudinalForceComponent;
use crate::components::longtitudinal_force::LongtitudinalForceComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::components::steering_angle::SteeringAngleComponent;
use crate::components::traction_force::TractionForceComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{Point, Vector};
use crate::rendering_context::RenderingContext;
use crate::utils::console_log;
use crate::world::{World, WorldObject};

pub fn render_car_physics_debug(world: &World, rendering_context: &mut RenderingContext) {
    for car in world.objects_by_component::<ControllableCarComponent>() {
        let position = car.component::<PositionComponent>().unwrap();
        let traction_force = car.component::<TractionForceComponent>().unwrap();
        let longtitudinal_force = car.component::<LongtitudinalForceComponent>().unwrap();
        let steering_angle = car.component::<SteeringAngleComponent>().unwrap();
        let latitudinal_force = car.component::<LatitudinalForceComponent>().unwrap();

        rendering_context.draw_debug_text(
            &Point::new(0.0, 10.0),
            &format!("Position: {:?}", position.point())
        );
        rendering_context.draw_debug_text(
            &Point::new(0.0, 20.0),
            &format!("Traction force: {:?}", traction_force.force()),
        );
        rendering_context.draw_debug_text(
            &Point::new(0.0, 30.0),
            &format!("Longtitudinal force: {:?}", longtitudinal_force.force())
        );
        rendering_context.draw_debug_text(
            &Point::new(0.0, 40.0),
            &format!("Steering angle: {:?}", steering_angle.angle())
        );
        rendering_context.draw_debug_text(
            &Point::new(0.0, 50.0),
            &format!("Latitudinal force: {:?}", latitudinal_force.force()),
        );
    }

    for collision_body in world.objects_by_component::<CollisionBodyComponent>() {
        let velocity = collision_body.component::<VelocityComponent>().unwrap();
        let position = collision_body.component::<PositionComponent>().unwrap();
        let dimensions = collision_body.component::<DimensionsComponent>().unwrap();
        let rotation = collision_body.component::<RotationComponent>().unwrap();
        let desired_velocity = collision_body.component::<DesiredVelocityComponent>();
        let collision_body = collision_body.component::<CollisionBodyComponent>().unwrap();

        let polygon = collision_body.polygon().rotated_by(rotation.angle());
        let center = polygon.center() + position.point();

        for point in polygon.points() {
            let point = point + position.point();
            rendering_context.draw_debug_point(&point);
        }

        rendering_context.draw_debug_line_with_color(&center, &(&center + velocity.velocity() * 100.0), &Color::blue());
        if let Some(desired_velocity) = desired_velocity {
            let desired_velocity = desired_velocity.desired_velocity();
            rendering_context.draw_debug_line_with_color(&center, &(&center + desired_velocity * 100.0), &Color::red());
        }
    }
}