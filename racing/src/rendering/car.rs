use crate::components::car::CarComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_cars(world: &World, rendering_context: &mut RenderingContext) {
    for car in world.objects_by_component::<CarComponent>() {
        let position = car.component::<PositionComponent>().unwrap();
        let dimensions = car.component::<DimensionsComponent>().unwrap();
        let rotation = car.component::<RotationComponent>().unwrap();
        let car = car.component::<CarComponent>().unwrap();

        rendering_context.draw_image_with_rotation(car.image(), position.point(), dimensions.dimensions(), rotation.angle());
    }
}