use crate::components::track::TrackComponent;
use crate::rendering_context::RenderingContext;
use crate::utils::console_log;
use crate::world::World;

pub fn render_track_debug(world: &World, rendering_context: &mut RenderingContext) {
    for track in world.objects_by_component::<TrackComponent>() {
        let track = track.component::<TrackComponent>().unwrap();

        for i in 0..track.route().len() {
            let j = if i == 0 {
                track.route().len() - 1
            } else {
                i - 1
            };
            rendering_context.draw_debug_point(&track.route()[i]);
            rendering_context.draw_debug_line(&track.route()[j], &track.route()[i]);
        }
    }
}