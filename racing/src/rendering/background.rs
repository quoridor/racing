use lazy_static::lazy_static;
use crate::components::track::TrackComponent;
use crate::geometry::{Point, Dimensions};
use crate::rendering_context::{RenderingContext, Image};
use crate::start;
use crate::world::World;

const TILE_SIZE: Dimensions = Dimensions::new(128.0, 128.0);

lazy_static! {
    pub static ref GRASS: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(910.0, 260.0),
        Dimensions::new(128.0, 128.0),
    );
}

pub fn render_background(_: &World, rendering_context: &mut RenderingContext) {
    let camera_position = rendering_context.camera_position();
    let rendering_context_dimensions = rendering_context.dimensions().clone();

    let start_x = ((camera_position.x()-rendering_context_dimensions.width()) as u32) / (TILE_SIZE.width() as u32) * (TILE_SIZE.width() as u32);
    let start_y = ((camera_position.y()-rendering_context_dimensions.height()) as u32) / (TILE_SIZE.height() as u32) * (TILE_SIZE.height() as u32);

    for x in (start_x..start_x + rendering_context_dimensions.width() as u32 * 2).step_by(TILE_SIZE.width() as usize) {
        for y in (start_y..start_y + rendering_context_dimensions.height() as u32 * 2).step_by(TILE_SIZE.height() as usize) {
            rendering_context.draw_image(&GRASS, &Point::new(x as f64, y as f64), &TILE_SIZE);
        }
    }
}