use crate::components::tile::TileComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_tiles(world: &World, rendering_context: &mut RenderingContext) {
    for tile in world.objects_by_component::<TileComponent>() {
        let tile = tile.component::<TileComponent>().unwrap();
        rendering_context.draw_image(tile.image(), tile.position(), tile.size());
    }
}