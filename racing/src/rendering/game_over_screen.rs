use crate::components::controllable_car::ControllableCarComponent;
use crate::components::eliminated_car::EliminatedCarComponent;
use crate::components::lap_counter::LapCounterComponent;
use crate::components::velocity::VelocityComponent;
use crate::rendering_context::RenderingContext;
use crate::world::World;

pub fn render_game_over_screen(world: &World, rendering_context: &mut RenderingContext) {
    let player_car = world.objects_by_component::<ControllableCarComponent>().next().unwrap();
    let player_car_is_eliminated = player_car.contains_component::<EliminatedCarComponent>();
    let player_car_velocity = player_car.component::<VelocityComponent>().unwrap().velocity().magnitude();

    if player_car_is_eliminated && player_car_velocity < 0.1 {
        let lap_counter = world.objects_by_component::<LapCounterComponent>()
            .next().unwrap()
            .component::<LapCounterComponent>()
            .unwrap()
            .total_laps();

        rendering_context.clear();
        rendering_context.draw_prompt_text(&format!("game over, total laps: {}", lap_counter));
    }
}