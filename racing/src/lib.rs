#![feature(box_syntax)]
#![feature(drain_filter)]

use std::cell::RefCell;
use std::f64;
use std::rc::Rc;
use rand::Rng;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use crate::components::ai_car::AiCarComponent;
use crate::components::attach_position::AttachPositionComponent;
use crate::components::camera::CameraComponent;
use crate::components::car::{CarComponent, CarType};
use crate::components::collision_body::CollisionBodyComponent;
use crate::components::collision_force::CollisionForceComponent;
use crate::components::controllable_car::ControllableCarComponent;
use crate::components::desired_velocity::DesiredVelocityComponent;
use crate::components::dimensions::DimensionsComponent;
use crate::components::drag_force::DragForceComponent;
use crate::components::engine_force::EngineForceComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::lap_counter::LapCounterComponent;
use crate::components::longtitudinal_force::LongtitudinalForceComponent;
use crate::components::max_engine_power::MaxEnginePowerComponent;
use crate::components::pedestrian::{PedestrianComponent, PedestrianType};
use crate::components::pedestrian_movement_resistance::PedestrianMovementResistanceComponent;
use crate::components::police_base::PoliceBaseComponent;
use crate::components::police_car::PoliceCarComponent;
use crate::components::position::PositionComponent;
use crate::components::rolling_resistance::RollingResistanceComponent;
use crate::components::rotation::RotationComponent;
use crate::components::rotation_movement::RotationMovementComponent;
use crate::components::tile::TileComponent;
use crate::components::track::TrackComponent;
use crate::components::traction_force::TractionForceComponent;
use crate::components::velocity::VelocityComponent;
use crate::extensions::car_physics::WithCarPhysics;
use crate::extensions::cars::WithCars;
use crate::extensions::pedestrians::WithPedestriansEverywhere;
use crate::extensions::police_cars_at_bases::WithPoliceCarsAtPoliceBases;
use crate::geometry::{Dimensions, Point, Polygon, Vector};
use crate::input::InputContext;
use crate::reducers::ai_car::ai_car_reducer;
use crate::reducers::apply_angular_velocity::apply_angular_velocity_reducer;
use crate::reducers::apply_velocity::apply_velocity_reducer;
use crate::reducers::attach_position::attach_position_reducer;
use crate::reducers::collisions::collisions_reducer;
use crate::reducers::controllable_car::controllable_car_reducer;
use crate::reducers::drag_force::drag_force_reducer;
use crate::reducers::elimitated_cars::eliminated_cars_reducer;
use crate::reducers::latitudinal_force::latitudinal_force_reducer;
use crate::reducers::longtitudinal_force::longtitudinal_force_reducer;
use crate::reducers::rolling_resistance::rolling_resistance_reducer;
use crate::reducers::rotation_movement::rotation_movement_reducer;
use crate::reducers::traction_force::traction_force_reducer;
use crate::reducers::velocity::velocity_reducer;
use crate::reducers::kill_pedestrian::kill_pedestrian_reducer;
use crate::reducers::maintain_desired_velocity::maintain_desired_velocity_reducer;
use crate::reducers::pedestrian_movement_resistance::pedestrian_movement_resistance_reducer;
use crate::reducers::police_car::police_car_reducer;
use crate::reducers::update_lap_counter::update_lap_counter_reducer;
use crate::reducers::walk_to_object::walk_to_object_reducer;
use crate::reducers::walk_to_target_position::walk_to_target_position_reducer;
use crate::rendering::background::render_background;
use crate::rendering::car::render_cars;
use crate::rendering::car_physics_debug::render_car_physics_debug;
use crate::rendering::game_over_screen::render_game_over_screen;
use crate::rendering::lap_counter::render_lap_counter;
use crate::rendering::pedestrian::render_pedestrians;
use crate::rendering::tile::render_tiles;
use crate::rendering::track::render_track;
use crate::rendering::track_debug::render_track_debug;
use crate::rendering_context::RenderingContext;
use crate::utils::{console_log, performance_now};
use crate::world::{World, WorldObject};

mod components;
mod extensions;
mod reducers;
mod rendering;

mod color;
mod geometry;
mod input;
mod rendering_context;
mod utils;
mod world;

type WorldReducer = Box<dyn Fn(&mut World, f64, &InputContext)>;
type RenderingPipelineElement = Box<dyn Fn(&World, &mut RenderingContext)>;

#[wasm_bindgen(start)]
pub fn start() {
    run_main_loop(
        create_world(),
        create_world_reducers(),
        create_rendering_pipeline(),
        RenderingContext::new(),
        InputContext::new(),
    );
}

fn create_world() -> World {
    let player_car_id = Id::new("player_car");

    World::empty()
        .with_object(WorldObject::new()
            .with_component(IdComponent::new(Id::new("camera")))
            .with_component(CameraComponent::new())
            .with_component(PositionComponent::new(Point::zero()))
            .with_component(AttachPositionComponent::new(player_car_id.clone())))
        .with_object(
            WorldObject::new()
                .with_component(IdComponent::new("TrackComponent1".into()))
                .with_component(TrackComponent::new()))
        .with_object(WorldObject::new()
            .with_component(PositionComponent::new(Point::new(6349.0, 5678.0)))
            .with_component(PoliceBaseComponent::new()))
        .with_object(WorldObject::new()
            .with_component(PositionComponent::new(Point::new(7433.0, 3152.0)))
            .with_component(PoliceBaseComponent::new()))
        .with_object(WorldObject::new()
            .with_component(PositionComponent::new(Point::new(2963.0, 1860.0)))
            .with_component(PoliceBaseComponent::new()))
        .with_object(WorldObject::new()
            .with_component(PositionComponent::new(Point::new(1426.0, 4811.0)))
            .with_component(PoliceBaseComponent::new()))
        .with_object(WorldObject::new()
            .with_component(PositionComponent::new(Point::new(3043.0, 6326.0)))
            .with_component(PoliceBaseComponent::new()))
        .with_police_car_at_each_police_base()
        .with_cars(player_car_id.clone())
        .with_pedestrians_everywhere()
}

fn create_world_reducers() -> Vec<WorldReducer> {
    vec![
        box controllable_car_reducer,
        box ai_car_reducer,
        box police_car_reducer,

        box maintain_desired_velocity_reducer,
        box walk_to_target_position_reducer,
        box walk_to_object_reducer,

        box eliminated_cars_reducer,

        box rotation_movement_reducer,
        box traction_force_reducer,
        box drag_force_reducer,
        box rolling_resistance_reducer,
        box pedestrian_movement_resistance_reducer,
        box longtitudinal_force_reducer,
        box latitudinal_force_reducer,
        box velocity_reducer,
        box apply_velocity_reducer,
        box apply_angular_velocity_reducer,
        box collisions_reducer,
        // box kill_pedestrian_reducer,

        box update_lap_counter_reducer,
        box attach_position_reducer,
    ]
}

fn create_rendering_pipeline() -> Vec<RenderingPipelineElement> {
    vec![
        box render_background,
        box render_track,
        box render_tiles,
        box render_pedestrians,
        box render_cars,

        box render_lap_counter,

        // box render_track_debug,
        // box render_car_physics_debug,

        box render_game_over_screen,
    ]
}

fn run_main_loop(
    mut world: World,
    world_reducers: Vec<WorldReducer>,
    rendering_pipleline: Vec<RenderingPipelineElement>,
    mut rendering_context: RenderingContext,
    input_context: InputContext,
) {
    let f: Rc<RefCell<Option<Closure<dyn FnMut()>>>> = Rc::new(RefCell::new(None));
    let g = f.clone();

    *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {
        let delta = rendering_context.compute_delta(performance_now());
        world_reducers.iter().for_each(|reducer| reducer(&mut world, delta, &input_context));

        rendering_context.on_new_frame(&world);
        rendering_pipleline.iter().for_each(|element| element(&world, &mut rendering_context));

        // request next frame render
        request_animation_frame(f.clone());
    }) as Box<dyn FnMut()>));

    // request first render
    request_animation_frame(g.clone());
}

fn request_animation_frame(closure: Rc<RefCell<Option<Closure<dyn FnMut()>>>>) {
    web_sys::window().unwrap()
        .request_animation_frame(closure.borrow().as_ref().unwrap().as_ref().unchecked_ref())
        .unwrap();
}
