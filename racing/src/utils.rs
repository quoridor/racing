pub fn console_log(text: &str) {
    let array = js_sys::Array::new();
    array.push(&text.into());
    web_sys::console::log(&array);
}

pub fn performance_now() -> f64 {
    web_sys::window().unwrap().performance().unwrap().now()
}