pub struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

impl Color {

    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Self {
            red,
            green,
            blue,
        }
    }

    pub fn red() -> Self {
        Self::new(255, 0, 0)
    }

    pub fn green() -> Self {
        Self::new(0, 255, 0)
    }

    pub fn blue() -> Self {
        Self::new(0, 0, 255)
    }

    pub fn to_css(&self) -> String {
        format!("rgb({}, {}, {})", self.red, self.green, self.blue)
    }
}