use crate::geometry::Vector;

pub struct LongtitudinalForceComponent {
    force: Vector,
}

impl LongtitudinalForceComponent {
    pub fn new() -> Self {
        Self {
            force: Vector::zero(),
        }
    }

    pub fn set(&mut self, force: Vector) {
        self.force = force;
    }

    pub fn force(&self) -> &Vector {
        &self.force
    }
}