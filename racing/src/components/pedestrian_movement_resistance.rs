use crate::geometry::Vector;

pub struct PedestrianMovementResistanceComponent {
    force: Vector,
}

impl PedestrianMovementResistanceComponent {
    pub fn new() -> Self {
        Self {
            force: Vector::zero(),
        }
    }

    pub fn set(&mut self, vector: Vector) {
        self.force = vector;
    }

    pub fn force(&self) -> &Vector {
        &self.force
    }
}