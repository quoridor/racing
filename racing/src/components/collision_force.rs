use crate::geometry::Vector;

pub struct CollisionForceComponent {
    force: Vector,
}

impl CollisionForceComponent {
    pub fn new() -> Self {
        Self {
            force: Vector::zero(),
        }
    }

    pub fn force(&self) -> &Vector {
        &self.force
    }

    pub fn set(&mut self, force: Vector) {
        self.force = force;
    }

    pub fn add(&mut self, force: Vector) {
        self.force += force;
    }
}