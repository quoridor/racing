use crate::geometry::Vector;

pub struct DesiredVelocityComponent {
    desired_velocity: Vector,
}

impl DesiredVelocityComponent {
    pub fn new() -> Self {
        Self {
            desired_velocity: Vector::zero(),
        }
    }

    pub fn desired_velocity(&self) -> &Vector {
        &self.desired_velocity
    }

    pub fn set(&mut self, desired_velocity: Vector) {
        self.desired_velocity = desired_velocity;
    }
}