use std::f64::consts::PI;
use crate::geometry::{Angle, Vector};

pub struct RotationComponent {
    angle: Angle,
}

impl RotationComponent {
    pub fn new(angle: f64) -> Self {
        Self {
            angle: Angle::from_degrees(angle),
        }
    }

    pub fn angle(&self) -> &Angle {
        &self.angle
    }

    pub fn add(&mut self, angle: &Angle) {
        self.angle += angle;
    }

    pub fn set(&mut self, angle: Angle) {
        self.angle = angle;
    }
}