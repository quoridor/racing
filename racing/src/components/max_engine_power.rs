pub struct MaxEnginePowerComponent {
    max_power: f64,
}

impl MaxEnginePowerComponent {
    pub fn new(max_power: f64) -> Self {
        Self {
            max_power,
        }
    }

    pub fn max_power(&self) -> f64 {
        self.max_power
    }
}