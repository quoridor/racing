use crate::geometry::Polygon;

pub struct CollisionBodyComponent {
    polygon: Polygon,
}

impl CollisionBodyComponent {
    pub fn new(polygon: Polygon) -> Self {
        Self {
            polygon,
        }
    }

    pub fn polygon(&self) -> &Polygon {
        &self.polygon
    }
}