use crate::geometry::Vector;

pub struct VelocityComponent {
    velocity: Vector,
}

impl VelocityComponent {
    pub fn new(velocity: Vector) -> Self {
        Self {
            velocity,
        }
    }

    pub fn zero() -> Self {
        Self::new(Vector::zero())
    }

    pub fn velocity(&self) -> &Vector {
        &self.velocity
    }

    pub fn set(&mut self, vector: Vector) {
        self.velocity = vector;
    }

    pub fn add(&mut self, vector: &Vector) {
        self.velocity += vector;
    }
}