use lazy_static::lazy_static;
use crate::geometry::{Dimensions, Point};
use crate::rendering_context::Image;

lazy_static! {
    pub static ref ASPHALT_FULL: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 650.0),
        Dimensions::new(128.0, 128.0),
    );
}

pub struct TileComponent {
    position: Point,
    size: Dimensions,
    image: Image,
}

impl TileComponent {
    pub fn new() -> Self {
        Self {
            position: Point::zero(),
            size: Dimensions::new(128.0, 128.0),
            image: ASPHALT_FULL.clone(),
        }
    }

    pub fn position(&self) -> &Point {
        &self.position
    }

    pub fn size(&self) -> &Dimensions {
        &self.size
    }

    pub fn image(&self) -> &Image {
        &self.image
    }
}