use crate::geometry::Angle;

pub struct SteeringAngleComponent {
    angle: Angle,
}

impl SteeringAngleComponent {
    pub fn new() -> Self {
        Self {
            angle: Angle::zero(),
        }
    }

    pub fn angle(&self) -> &Angle {
        &self.angle
    }

    pub fn set_angle(&mut self, angle: Angle) {
        self.angle = angle.min(Angle::from_degrees(30.0)).max(Angle::from_degrees(-30.0));
    }

    pub fn add_angle(&mut self, angle: &Angle) {
        self.angle += angle;
    }
}