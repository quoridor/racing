use lazy_static::lazy_static;
use crate::geometry::{Dimensions, Point};
use crate::rendering_context::Image;

lazy_static! {
    static ref BLUE_BLONDE: Image = Image::new(
        "/assets/spritesheet_characters.png".to_owned(),
        Point::new(53.0, 190.0),
        Dimensions::new(55.0, 55.0)
    );
}

pub enum PedestrianType {
    PedestrianBlueBlond
}

pub struct PedestrianComponent {
    image: Image,
}

impl PedestrianComponent {
    pub fn new(car_type: PedestrianType) -> Self {
        match car_type {
            PedestrianType::PedestrianBlueBlond => Self { image: BLUE_BLONDE.clone() }
        }
    }

    pub fn image(&self) -> &Image {
        &self.image
    }
}

