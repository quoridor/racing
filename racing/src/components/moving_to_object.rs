use crate::components::id::Id;

pub struct MovingToObjectComponent {
    object_id: Id,
}

impl MovingToObjectComponent {
    pub fn new(object_id: Id) -> Self {
        Self {
            object_id,
        }
    }

    pub fn object_id(&self) -> &Id {
        &self.object_id
    }
}