pub struct LapCounterComponent {
    total_laps: u32,
    next_checkpoint: u32,
}

impl LapCounterComponent {
    pub fn new() -> Self {
        Self {
            total_laps: 0,
            next_checkpoint: 2,
        }
    }

    pub fn total_laps(&self) -> u32 {
        self.total_laps
    }

    pub fn next_checkpoint(&self) -> u32 {
        self.next_checkpoint
    }

    pub fn set_total_laps(&mut self, laps: u32) {
        self.total_laps = laps;
    }

    pub fn set_next_checkpoint(&mut self, next_checkpoint: u32) {
        self.next_checkpoint = next_checkpoint;
    }
}