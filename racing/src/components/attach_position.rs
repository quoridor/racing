use crate::components::id::Id;

pub struct AttachPositionComponent {
    attach_to: Id,
}

impl AttachPositionComponent {
    pub fn new(attach_to: Id) -> Self {
        Self {
            attach_to,
        }
    }

    pub fn attach_to(&self) -> &Id {
        &self.attach_to
    }
}