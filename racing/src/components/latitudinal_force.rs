use crate::geometry::Vector;

pub struct LatitudinalForceComponent {
    force: Vector,
}

impl LatitudinalForceComponent {
    pub fn new() -> Self {
        Self {
            force: Vector::zero(),
        }
    }

    pub fn force(&self) -> &Vector {
        &self.force
    }

    pub fn set(&mut self, force: Vector) {
        self.force = force;
    }
}