use crate::geometry::Dimensions;

pub struct DimensionsComponent {
    dimensions: Dimensions,
}

impl DimensionsComponent {
    pub fn new(dimensions: Dimensions) -> Self {
        Self {
            dimensions,
        }
    }

    pub fn dimensions(&self) -> &Dimensions {
        &self.dimensions
    }
}