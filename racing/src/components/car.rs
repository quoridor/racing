use lazy_static::lazy_static;
use crate::geometry::{Dimensions, Point};
use crate::rendering_context::Image;

lazy_static! {
    static ref BLUE_CAR: Image = Image::new(
        "/assets/spritesheet_vehicles.png".to_owned(),
        Point::new(0.0, 251.0),
        Dimensions::new(71.0, 131.0)
    );

    static ref RED_CAR: Image = Image::new(
        "/assets/spritesheet_vehicles.png".to_owned(),
        Point::new(73.0, 251.0),
        Dimensions::new(69.0, 114.0)
    );

    static ref GREEN_CAR: Image = Image::new(
        "/assets/spritesheet_vehicles.png".to_owned(),
        Point::new(73.0, 0.0),
        Dimensions::new(71.0, 131.0)
    );

    static ref YELLOW_CAR: Image = Image::new(
        "/assets/spritesheet_vehicles.png".to_owned(),
        Point::new(146.0, 118.0),
        Dimensions::new(71.0, 131.0)
    );

    static ref BLACK_CAR: Image = Image::new(
        "/assets/spritesheet_vehicles.png".to_owned(),
        Point::new(290.0, 135.0),
        Dimensions::new(69.0, 127.0)
    );

    static ref POLICE_CAR: Image = Image::new(
        "/assets/police_car.png".to_owned(),
        Point::new(1.0, 0.0),
        Dimensions::new(69.0, 127.0)
    );
}

#[derive(Clone)]
pub enum CarType {
    BlueCar,
    RedCar,
    GreenCar,
    YellowCar,
    BlackCar,
    PoliceCar,
}

pub struct CarComponent {
    image: Image,
}

impl CarComponent {
    pub fn new(car_type: CarType) -> Self {
        match car_type {
            CarType::BlueCar => Self { image: BLUE_CAR.clone() },
            CarType::RedCar => Self { image: RED_CAR.clone() },
            CarType::GreenCar => Self { image: GREEN_CAR.clone() },
            CarType::YellowCar => Self { image: YELLOW_CAR.clone() },
            CarType::PoliceCar => Self { image: POLICE_CAR.clone() },
            CarType::BlackCar => Self {image: BLACK_CAR.clone()}
        }
    }

    pub fn image(&self) -> &Image {
        &self.image
    }
}



