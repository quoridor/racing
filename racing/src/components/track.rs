use std::collections::{HashMap, HashSet};
use lazy_static::lazy_static;
use crate::geometry::{Dimensions, Point, Polygon, Segment, Vector};
use crate::rendering_context::Image;

const TILE_SIZE: Dimensions = Dimensions::new(128.0, 128.0);

lazy_static! {
    pub static ref GRASS: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(910.0, 260.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_FULL: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 650.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_BORDER_SOUTH: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 260.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_BORDER_NORTH: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2600.0, 1040.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_BORDER_EAST: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 520.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_BORDER_WEST: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 780.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_CONNECTOR_FULL_LEFT_HALF_RIGHT: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 780.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_CONNECTOR_HALF_LEFT_NONE_RIGHT: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 650.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_CONNECTOR_BOTTOM_RIGHT: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1040.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_BOTTOM_LEFT: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2080.0, 910.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_FULL_LEFT_SMOOTH_TURN_BOTTOM_LEFT_RIGHT: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2080.0, 1040.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_0: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2600.0, 520.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_1: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2600.0, 390.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_2: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2600.0, 260.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_3: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2600.0, 130.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_4: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 130.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_5: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 0.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_6: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1820.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_7: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1690.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_8: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 1690.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_9: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 1560.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_10: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 1430.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_11: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 1300.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_12: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2080.0, 1300.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_SMOOTH_TURN_13: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2080.0, 1170.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_0: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2600.0, 0.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_1: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 1820.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_2: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 1690.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_3: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1560.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_4: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1430.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_5: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1300.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_0: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 1560.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_1: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 1430.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_2: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 1300.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_3: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2470.0, 1170.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_4: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1170.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_5: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 1170.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_6: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 910.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_7: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2340.0, 780.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_10: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 520.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_DIAGONAL_V_11: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(2210.0, 390.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_FINISH_LINE_BORDER_NORTH: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(1950.0, 0.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_FINISH_LINE_VERTICAL: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(1820.0, 1820.0),
        Dimensions::new(128.0, 128.0),
    );
    pub static ref ASPHALT_FINISH_LINE_BORDER_SOUTH: Image = Image::new(
        "/assets/spritesheet_tiles.png".to_owned(),
        Point::new(1820.0, 1690.0),
        Dimensions::new(128.0, 128.0),
    );
}

pub struct TrackComponent {
    route: Vec<Point>,
    finish_line: Segment,
    tiles: Vec<TrackTile>,
}

#[derive(Clone)]
pub struct TrackTile {
    position: Point,
    tile_type: TileType,
}

#[derive(Clone, Eq, PartialEq)]
enum TileType {
    Grass,
    Full,
    BorderSouth,
    BorderNorth,
    BorderEast,
    BorderWest,
    ConnectorFullLeftHalfRight,
    ConnectorHalfLeftNoneRight,
    ConnectorBottomRight,
    SmoothTurnBottomLeft,
    FullLeftSmoothTurnBottomLeftRight,
    SmoothTurn0,
    SmoothTurn1,
    SmoothTurn2,
    SmoothTurn3,
    SmoothTurn4,
    SmoothTurn5,
    SmoothTurn6,
    SmoothTurn7,
    SmoothTurn8,
    SmoothTurn9,
    SmoothTurn10,
    SmoothTurn11,
    SmoothTurn12,
    SmoothTurn13,
    Diagonal0,
    Diagonal1,
    Diagonal2,
    Diagonal3,
    Diagonal4,
    Diagonal5,
    DiagonalV0,
    DiagonalV1,
    DiagonalV2,
    DiagonalV3,
    DiagonalV4,
    DiagonalV6,
    DiagonalV7,
    DiagonalV10,
    DiagonalV11,
    FinishLineBorderNorth,
    FinishLineVertical,
    FinishLineBorderSouth,
}

impl TrackComponent {
    pub fn new() -> Self {
        let route = vec![
            Point::new(1050.0, 6240.0),
            Point::new(4400.0, 6100.0),
            Point::new(7410.0, 6020.0),
            Point::new(7830.0, 2660.0),
            Point::new(4240.0, 1310.0),
            Point::new(1200.0, 2030.0),
        ];
        let finish_line = Segment::new((
            Point::new(4400.0, 5500.0),
            Point::new(4400.0, 6500.0),
        ));
        let tiles = tiles_from_route(&route, &finish_line);

        Self {
            route,
            finish_line,
            tiles,
        }
    }

    pub fn route(&self) -> &Vec<Point> {
        &self.route
    }

    pub fn tiles(&self) -> &Vec<TrackTile> {
        &self.tiles
    }
}

impl TrackTile {

    pub fn position(&self) -> &Point {
        &self.position
    }

    pub fn image(&self) -> &Image {
        match &self.tile_type {
            TileType::Grass => &GRASS,
            TileType::Full => &ASPHALT_FULL,
            TileType::BorderSouth => &ASPHALT_BORDER_SOUTH,
            TileType::BorderNorth => &ASPHALT_BORDER_NORTH,
            TileType::BorderEast => &ASPHALT_BORDER_EAST,
            TileType::BorderWest => &ASPHALT_BORDER_WEST,
            TileType::ConnectorFullLeftHalfRight => &ASPHALT_CONNECTOR_FULL_LEFT_HALF_RIGHT,
            TileType::ConnectorHalfLeftNoneRight => &ASPHALT_CONNECTOR_HALF_LEFT_NONE_RIGHT,
            TileType::ConnectorBottomRight => &ASPHALT_CONNECTOR_BOTTOM_RIGHT,
            TileType::SmoothTurnBottomLeft => &ASPHALT_SMOOTH_TURN_BOTTOM_LEFT,
            TileType::FullLeftSmoothTurnBottomLeftRight => &ASPHALT_FULL_LEFT_SMOOTH_TURN_BOTTOM_LEFT_RIGHT,
            TileType::SmoothTurn0 => &ASPHALT_SMOOTH_TURN_0,
            TileType::SmoothTurn1 => &ASPHALT_SMOOTH_TURN_1,
            TileType::SmoothTurn2 => &ASPHALT_SMOOTH_TURN_2,
            TileType::SmoothTurn3 => &ASPHALT_SMOOTH_TURN_3,
            TileType::SmoothTurn4 => &ASPHALT_SMOOTH_TURN_4,
            TileType::SmoothTurn5 => &ASPHALT_SMOOTH_TURN_5,
            TileType::SmoothTurn6 => &ASPHALT_SMOOTH_TURN_6,
            TileType::SmoothTurn7 => &ASPHALT_SMOOTH_TURN_7,
            TileType::SmoothTurn8 => &ASPHALT_SMOOTH_TURN_8,
            TileType::SmoothTurn9 => &ASPHALT_SMOOTH_TURN_9,
            TileType::SmoothTurn10 => &ASPHALT_SMOOTH_TURN_10,
            TileType::SmoothTurn11 => &ASPHALT_SMOOTH_TURN_11,
            TileType::SmoothTurn12 => &ASPHALT_SMOOTH_TURN_12,
            TileType::SmoothTurn13 => &ASPHALT_SMOOTH_TURN_13,
            TileType::Diagonal0 => &ASPHALT_DIAGONAL_0,
            TileType::Diagonal1 => &ASPHALT_DIAGONAL_1,
            TileType::Diagonal2 => &ASPHALT_DIAGONAL_2,
            TileType::Diagonal3 => &ASPHALT_DIAGONAL_3,
            TileType::Diagonal4 => &ASPHALT_DIAGONAL_4,
            TileType::Diagonal5 => &ASPHALT_DIAGONAL_5,
            TileType::DiagonalV0 => &ASPHALT_DIAGONAL_V_0,
            TileType::DiagonalV1 => &ASPHALT_DIAGONAL_V_1,
            TileType::DiagonalV2 => &ASPHALT_DIAGONAL_V_2,
            TileType::DiagonalV3 => &ASPHALT_DIAGONAL_V_3,
            TileType::DiagonalV4 => &ASPHALT_DIAGONAL_V_4,
            TileType::DiagonalV6 => &ASPHALT_DIAGONAL_V_6,
            TileType::DiagonalV7 => &ASPHALT_DIAGONAL_V_7,
            TileType::DiagonalV10 => &ASPHALT_DIAGONAL_V_10,
            TileType::DiagonalV11 => &ASPHALT_DIAGONAL_V_11,
            TileType::FinishLineBorderNorth => &ASPHALT_FINISH_LINE_BORDER_NORTH,
            TileType::FinishLineVertical => &ASPHALT_FINISH_LINE_VERTICAL,
            TileType::FinishLineBorderSouth => &ASPHALT_FINISH_LINE_BORDER_SOUTH,
        }
    }

    pub fn size(&self) -> &Dimensions {
        &TILE_SIZE
    }
}

fn tiles_from_route(route: &Vec<Point>, finish_line_segment: &Segment) -> Vec<TrackTile> {
    let mut route_segments = Vec::new();
    for i in 0..route.len() {
        let t = if i == 0 {
            route.len() - 1
        } else {
            i - 1
        };

        route_segments.push(Segment::new((route[t].clone(), route[i].clone())));
    }

    let min_x = route.iter()
        .map(|p| p.x())
        .reduce(|a, b| if a < b { a } else { b })
        .unwrap() - 10.0* TILE_SIZE.width();
    let min_y = route.iter()
        .map(|p| p.y())
        .reduce(|a, b| if a < b { a } else { b })
        .unwrap() - 10.0 * TILE_SIZE.height();
    let max_x = route.iter()
        .map(|p| p.x())
        .reduce(|a, b| if a > b { a } else { b })
        .unwrap() + 10.0 * TILE_SIZE.width();
    let max_y = route.iter()
        .map(|p| p.y())
        .reduce(|a, b| if a > b { a } else { b })
        .unwrap() + 10.0 * TILE_SIZE.height();
    let track_width = max_x - min_x;
    let track_height = max_y - min_y;
    let total_tiles_x = (track_width / TILE_SIZE.width()).ceil() as u32;
    let total_tiles_y = (track_height / TILE_SIZE.height()).ceil() as u32;

    let mut tiles: HashMap<(u32, u32), TrackTile> = HashMap::new();

    // First pass: place base tiles
    for x in 0..total_tiles_x {
        let world_x = x as f64 * TILE_SIZE.width() + min_x;

        for y in 0..total_tiles_y {
            let world_y = y as f64 * TILE_SIZE.height() + min_y;

            if tiles.contains_key(&(x, y)) {
                continue;
            }

            let tile_polygon = Polygon::rectangle(TILE_SIZE.clone())
                .translated_by(&Vector::new(world_x, world_y));
            for segment in &route_segments {
                if tile_polygon.intersects_segment(segment) {
                    for dx in -1i32..=1 {
                        for dy in -1i32..=1 {
                            let x = (x as i32 + dx) as u32;
                            let y = (y as i32 + dy) as u32;

                            if tiles.contains_key(&(x, y)) {
                                continue;
                            }

                            let world_x = x as f64 * TILE_SIZE.width() + min_x;
                            let world_y = y as f64 * TILE_SIZE.height() + min_y;

                            tiles.insert((x, y), TrackTile {
                                position: Point::new(world_x, world_y),
                                tile_type: TileType::Full,
                            });
                        }
                    }
                    break;
                }
            }
        }
    }

    // Second pass: borders
    for x in 0..total_tiles_x {
        let world_x = x as f64 * TILE_SIZE.width() + min_x;

        for y in 0..total_tiles_y {
            let world_y = y as f64 * TILE_SIZE.height() + min_y;

            if tiles.contains_key(&(x, y)) {
                continue;
            }

            if y > 0 {
                let top_tile = (x, y - 1);
                if tiles.contains_key(&top_tile) && tiles[&top_tile].tile_type == TileType::Full {
                    tiles.get_mut(&top_tile).unwrap().tile_type = TileType::BorderSouth;
                }
            }

            if x > 0 {
                let left_tile = (x - 1, y);
                if tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::Full {
                    tiles.get_mut(&left_tile).unwrap().tile_type = TileType::BorderEast;
                }
            }

            let bottom_tile = (x, y + 1);
            if tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::Full {
                tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::BorderNorth;
            }

            let right_tile = (x + 1, y);
            if tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::Full {
                tiles.get_mut(&right_tile).unwrap().tile_type = TileType::BorderWest;
            }
        }
    }

    // Third pass: connectors
    for i in 0..8 {
        for x in 0..total_tiles_x {
            let world_x = x as f64 * TILE_SIZE.width() + min_x;

            for y in 0..total_tiles_y {
                let world_y = y as f64 * TILE_SIZE.height() + min_y;

                let this_tile = (x, y);
                let right_tile = (x + 1, y);
                let right2_tile = (x + 2, y);
                let bottom_tile = (x, y + 1);

                let this_tile_is_full = tiles.contains_key(&this_tile) && tiles[&this_tile].tile_type == TileType::Full;
                let this_tile_is_south_border = tiles.contains_key(&this_tile) && tiles[&this_tile].tile_type == TileType::BorderSouth;
                let this_tile_is_north_border = tiles.contains_key(&this_tile) && tiles[&this_tile].tile_type == TileType::BorderNorth;
                let this_tile_is_west_border = tiles.contains_key(&this_tile) && tiles[&this_tile].tile_type == TileType::BorderWest;
                let this_tile_is_east_border = tiles.contains_key(&this_tile) && tiles[&this_tile].tile_type == TileType::BorderEast;
                let this_tile_is_smooth_turn7 = tiles.contains_key(&this_tile) && tiles[&this_tile].tile_type == TileType::SmoothTurn7;
                let right_tile_is_south_border = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::BorderSouth;
                let right_tile_is_west_border = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::BorderWest;
                let right_tile_is_north_border = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::BorderNorth;
                let right_tile_is_smooth_turn_bottom_left = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::SmoothTurnBottomLeft;
                let right_tile_is_smooth_turn3 = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::SmoothTurn3;
                let right_tile_is_smooth_turn7 = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::SmoothTurn7;
                let right2_tile_is_north_border = tiles.contains_key(&right2_tile) && tiles[&right2_tile].tile_type == TileType::BorderNorth;
                let bottom_tile_is_east_border = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::BorderEast;
                let bottom_tile_is_west_border = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::BorderWest;
                let bottom_tile_is_half_left_none_right_connector = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::ConnectorHalfLeftNoneRight;
                let bottom_tile_is_smooth_turn_bottom_left = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::SmoothTurnBottomLeft;
                let bottom_tile_is_smooth_turn2 = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::SmoothTurn2;
                let bottom_tile_is_smooth_turn11 = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::SmoothTurn11;
                let bottom_tile_is_smooth_turn12 = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::SmoothTurn12;
                let bottom_tile_is_diagonal5 = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::Diagonal5;
                let bottom_tile_is_diagonal_v10 = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::DiagonalV10;

                if y > 0 {
                    let top_tile = (x, y - 1);
                    let top_right_tile = (x + 1, y - 1);

                    let top_tile_is_east_border = tiles.contains_key(&top_tile) && tiles[&top_tile].tile_type == TileType::BorderEast;
                    let top_tile_is_west_border = tiles.contains_key(&top_tile) && tiles[&top_tile].tile_type == TileType::BorderWest;
                    let top_tile_is_south_border = tiles.contains_key(&top_tile) && tiles[&top_tile].tile_type == TileType::BorderSouth;
                    let top_right_tile_is_west_border = tiles.contains_key(&top_right_tile) && tiles[&top_right_tile].tile_type == TileType::BorderWest;

                    if this_tile_is_south_border && top_tile_is_east_border {
                        tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurnBottomLeft;
                    } else if this_tile_is_west_border && top_tile_is_west_border && right_tile_is_south_border {
                        tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn12;
                    } else if this_tile_is_north_border && top_right_tile_is_west_border {
                        tiles.get_mut(&right_tile).unwrap().tile_type = TileType::SmoothTurn10;
                    } else if top_tile_is_east_border && right_tile_is_north_border && right2_tile_is_north_border {
                        tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn9;
                    }
                }

                if x > 0 {
                    let left_tile = (x - 1, y);
                    let left_tile_is_west_border = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::BorderWest;
                    let left_tile_is_south_border = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::BorderSouth;
                    let left_tile_is_diagonal4 = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::Diagonal4;
                    let left_tile_is_smooth_turn12 = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::SmoothTurn12;
                    if left_tile_is_diagonal4 {
                        tiles.get_mut(&this_tile).unwrap().tile_type = TileType::Diagonal5;
                    } else if left_tile_is_smooth_turn12 && this_tile_is_south_border {
                        tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn13;
                    } else if left_tile_is_south_border && bottom_tile_is_west_border {
                        tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn6;
                    } else if this_tile_is_south_border && left_tile_is_west_border {
                        tiles.get_mut(&this_tile).unwrap().tile_type = TileType::DiagonalV11;
                        tiles.get_mut(&left_tile).unwrap().tile_type = TileType::DiagonalV10;
                    }
                }

                if this_tile_is_full && right_tile_is_south_border && bottom_tile_is_half_left_none_right_connector {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::ConnectorBottomRight;
                } else if this_tile_is_south_border && right_tile_is_smooth_turn_bottom_left {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::FullLeftSmoothTurnBottomLeftRight;
                } else if this_tile_is_east_border && bottom_tile_is_smooth_turn_bottom_left {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn11;
                } else if bottom_tile_is_smooth_turn11 && right_tile_is_smooth_turn_bottom_left {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::Diagonal4;
                } else if bottom_tile_is_diagonal5 && tiles.contains_key(&this_tile) {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::Diagonal2;
                } else if bottom_tile_is_east_border && right_tile_is_smooth_turn_bottom_left {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::Diagonal4;
                } else if this_tile_is_east_border && right_tile_is_smooth_turn7 {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::Full;
                } else if right_tile_is_smooth_turn3 {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn2;
                } else if this_tile_is_smooth_turn7 && bottom_tile_is_smooth_turn2 {
                    tiles.remove(&this_tile);
                } else if this_tile_is_west_border && bottom_tile_is_smooth_turn12 {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn8;
                } else if bottom_tile_is_diagonal_v10 {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::DiagonalV6;
                } else if bottom_tile_is_east_border && right_tile_is_south_border {
                    tiles.get_mut(&this_tile).unwrap().tile_type = TileType::SmoothTurn5;
                }

                if tiles.contains_key(&this_tile) {
                    continue;
                }

                if x > 0 && y > 0 {
                    let top_tile = (x, y - 1);
                    let left_tile = (x - 1, y);
                    let right_tile = (x + 1, y);
                    let right2_tile = (x + 2, y);
                    let bottom_tile = (x, y + 1);
                    let bottom2_tile = (x, y + 2);

                    let top_is_south_border = tiles.contains_key(&top_tile) && tiles[&top_tile].tile_type == TileType::BorderSouth;
                    let left_is_south_border = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::BorderSouth;
                    let left_is_east_border = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::BorderEast;
                    let left_is_north_border = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::BorderNorth;
                    let left_is_full_left_half_right_connector = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::ConnectorFullLeftHalfRight;
                    let left_is_diagonalv2 = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::DiagonalV2;
                    let left_is_diagonalv7 = tiles.contains_key(&left_tile) && tiles[&left_tile].tile_type == TileType::DiagonalV7;
                    let bottom_is_north_border = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::BorderNorth;
                    let bottom_is_west_border = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::BorderWest;
                    let bottom2_is_east_border = tiles.contains_key(&bottom2_tile) && tiles[&bottom2_tile].tile_type == TileType::BorderEast;
                    let bottom_is_smooth_turn_7 = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::SmoothTurn7;
                    let bottom_is_diagonal3 = tiles.contains_key(&bottom_tile) && tiles[&bottom_tile].tile_type == TileType::Diagonal3;
                    let right_is_west_border = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::BorderWest;
                    let right_is_diagonalv1 = tiles.contains_key(&right_tile) && tiles[&right_tile].tile_type == TileType::DiagonalV1;
                    let right2_is_north_border = tiles.contains_key(&right2_tile) && tiles[&right2_tile].tile_type == TileType::BorderNorth;

                    if top_is_south_border && left_is_south_border {
                        tiles.get_mut(&top_tile).unwrap().tile_type = TileType::Full;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::ConnectorFullLeftHalfRight,
                        });
                    } else if top_is_south_border && left_is_full_left_half_right_connector {
                        tiles.get_mut(&top_tile).unwrap().tile_type = TileType::Full;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::ConnectorHalfLeftNoneRight,
                        });
                    } else if bottom_is_north_border && bottom2_is_east_border {
                        tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::BorderEast;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::SmoothTurn7,
                        });
                    } else if bottom_is_smooth_turn_7 && left_is_east_border {
                        tiles.get_mut(&left_tile).unwrap().tile_type = TileType::Full;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::SmoothTurn3,
                        });
                    } else if left_is_north_border && bottom_is_north_border {
                        tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::Full;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::DiagonalV2,
                        });
                    } else if left_is_diagonalv2 && bottom_is_north_border {
                        tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::DiagonalV7;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::DiagonalV3,
                        });
                    } else if left_is_diagonalv7 && bottom_is_north_border {
                        tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::Full;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::DiagonalV2,
                        });
                    } else if bottom_is_north_border && right_is_west_border && right2_is_north_border {
                        tiles.get_mut(&right_tile).unwrap().tile_type = TileType::BorderNorth;
                        tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::Full;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::DiagonalV1,
                        });
                    } else if bottom_is_north_border && right_is_diagonalv1 {
                        tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::DiagonalV4;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::DiagonalV0,
                        });
                    } else if bottom_is_west_border && right_is_diagonalv1 {
                        tiles.get_mut(&right_tile).unwrap().tile_type = TileType::SmoothTurn1;
                        tiles.get_mut(&bottom_tile).unwrap().tile_type = TileType::SmoothTurn4;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::SmoothTurn0,
                        });
                    } else if bottom_is_west_border && right_is_west_border {
                        tiles.get_mut(&right_tile).unwrap().tile_type = TileType::Full;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::Diagonal3,
                        });
                    } else if bottom_is_diagonal3 && right_is_west_border {
                        tiles.get_mut(&right_tile).unwrap().tile_type = TileType::Diagonal1;
                        tiles.insert(this_tile, TrackTile {
                            position: Point::new(world_x, world_y),
                            tile_type: TileType::Diagonal0,
                        });
                    }
                }
            }
        }
    }

    // Fourth pass: finish line
    for x in 0..total_tiles_x {
        let world_x = x as f64 * TILE_SIZE.width() + min_x;

        for y in 0..total_tiles_y {
            let world_y = y as f64 * TILE_SIZE.height() + min_y;

            if !tiles.contains_key(&(x, y)) {
                continue;
            }

            let this_tile = (x, y);

            let tile_polygon = Polygon::rectangle(TILE_SIZE.clone())
                .translated_by(&Vector::new(world_x, world_y));

            if tile_polygon.intersects_segment(&finish_line_segment) {
                let tile_type = match tiles.get(&this_tile).unwrap().tile_type {
                    TileType::BorderNorth => TileType::FinishLineBorderNorth,
                    TileType::Full => TileType::FinishLineVertical,
                    TileType::BorderSouth => TileType::FinishLineBorderSouth,
                    _ => TileType::Grass,
                };

                tiles.get_mut(&this_tile).unwrap().tile_type = tile_type;
            }
        }
    }

    tiles.into_values().into_iter().collect()
}