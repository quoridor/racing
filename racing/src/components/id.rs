pub struct IdComponent {
    id: Id,
}

#[derive(Eq, PartialEq, Clone, Debug, Hash)]
pub struct Id {
    id: String,
}

impl From<&str> for Id {
    fn from(id: &str) -> Self {
        Self {
            id: id.to_owned()
        }
    }
}

impl IdComponent {
    pub fn new(id: Id) -> Self {
        Self {
            id,
        }
    }

    pub fn id(&self) -> &Id {
        &self.id
    }
}

impl Id {
    pub fn new<S: AsRef<str>>(id: S) -> Self {
        Self {
            id: id.as_ref().to_owned(),
        }
    }
}