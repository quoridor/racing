use crate::geometry::Vector;

pub struct RollingResistanceComponent {
    force: Vector,
}

impl RollingResistanceComponent {
    pub fn new() -> Self {
        Self {
            force: Vector::zero(),
        }
    }

    pub fn set(&mut self, vector: Vector) {
        self.force = vector;
    }

    pub fn force(&self) -> &Vector {
        &self.force
    }
}