pub struct AiCarComponent {
    next_track_point: u32,
}

impl AiCarComponent {
    pub fn new() -> Self {
        Self {
            next_track_point: 1,
        }
    }

    pub fn next_track_point(&self) -> u32 {
        self.next_track_point
    }

    pub fn set_next_track_point(&mut self, next_track_point: u32) {
        self.next_track_point = next_track_point;
    }
}