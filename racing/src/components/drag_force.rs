use crate::geometry::Vector;

pub struct DragForceComponent {
    force: Vector,
}

impl DragForceComponent {
    pub fn new() -> Self {
        Self {
            force: Vector::zero(),
        }
    }

    pub fn set(&mut self, force: Vector) {
        self.force = force;
    }

    pub fn force(&self) -> &Vector {
        &self.force
    }
}