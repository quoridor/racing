pub struct EngineForceComponent {
    force: f64,
}

impl EngineForceComponent {
    pub fn new() -> Self {
        Self {
            force: 0.0,
        }
    }

    pub fn set(&mut self, force: f64) {
        self.force = force;
    }

    pub fn force(&self) -> f64 {
        self.force
    }
}