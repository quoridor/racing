use crate::components::id::Id;

#[derive(Clone)]
pub struct PoliceCarComponent {
    following_car: Option<Id>,
}

impl PoliceCarComponent {
    pub fn new() -> Self {
        Self {
            following_car: None,
        }
    }

    pub fn following_car(&self) -> Option<&Id> {
        self.following_car.as_ref()
    }

    pub fn set_following_car(&mut self, car_id: Id) {
        self.following_car = Some(car_id);
    }

    pub fn clear_following_car(&mut self) {
        self.following_car = None;
    }
}