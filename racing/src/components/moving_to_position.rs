use crate::geometry::Point;

pub struct MovingToPositionComponent {
    target_position: Point,
}

impl MovingToPositionComponent {
    pub fn new(target_position: Point) -> Self {
        Self {
            target_position,
        }
    }

    pub fn set(&mut self, target_position: Point) {
        self.target_position = target_position;
    }

    pub fn target_position(&self) -> &Point {
        &self.target_position
    }
}