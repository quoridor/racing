use crate::components::car::CarComponent;
use crate::components::pedestrian::PedestrianComponent;
use crate::components::position::PositionComponent;
use crate::geometry::Point;
use crate::input::InputContext;
use crate::world::World;
use crate::components::id::IdComponent;

const DELTA: f64 = 20.0;

pub fn kill_pedestrian_reducer(world: &mut World, _delta: f64, _: &InputContext) {
    let pedestrians = world
        .objects_by_component::<PedestrianComponent>();

    let mut to_kill = Vec::new();
    {
        let cars = world
            .objects_by_component::<CarComponent>()
            .map(|c| c.component::<PositionComponent>().unwrap())
            .collect::<Vec<_>>();
        for pedestrian in pedestrians {
            let pedestrian_position = pedestrian.component::<PositionComponent>().unwrap();
            let pedestrian_id = pedestrian.component::<IdComponent>().unwrap();
            for car in cars.iter() {
                let distance = pedestrian_position.distance_to(car);
                if distance < DELTA {
                    to_kill.push(pedestrian_id.id().clone());
                }
            }
        }
    }

    world.remove_objects(&to_kill);
}
