use crate::components::pedestrian_movement_resistance::PedestrianMovementResistanceComponent;
use crate::components::velocity::VelocityComponent;
use crate::input::InputContext;
use crate::world::World;

const C_RESISTANCE: f64 = 8.0;

pub fn pedestrian_movement_resistance_reducer(world: &mut World, _delta: f64, _input: &InputContext) {
    world.objects_by_component_mut::<PedestrianMovementResistanceComponent>()
        .for_each(|obj| {
            let velocity = obj.component::<VelocityComponent>().unwrap().velocity().clone();
            if velocity.is_nan() {
                return;
            }

            let resistance = -C_RESISTANCE * velocity;
            obj.component_mut::<PedestrianMovementResistanceComponent>().unwrap().set(resistance);
        })
}