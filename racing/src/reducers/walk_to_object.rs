use std::collections::HashMap;
use rand::prelude::IteratorRandom;
use crate::components::car::CarComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::moving_to_object::MovingToObjectComponent;
use crate::components::moving_to_position::MovingToPositionComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::components::walking_force::WalkingForce;
use crate::geometry::{DELTA, Point, Vector};
use crate::input::InputContext;
use crate::world::World;

pub fn walk_to_object_reducer(world: &mut World, _: f64, _: &InputContext) {
    let car_positions: Vec<Point> = world.objects_by_component::<CarComponent>()
        .map(|v| v.component::<PositionComponent>().unwrap().point().clone())
        .collect();

    let mut position_by_id: HashMap<Id, Point> = HashMap::new();
    for obj in world.objects_by_component::<IdComponent>() {
        let id = obj.component::<IdComponent>().unwrap().id().clone();
        let pos = match obj.component::<PositionComponent>() {
            Some(v) => v.point().clone(),
            None => continue
        };

        position_by_id.insert(id, pos);
    }

    world.objects_by_component_mut::<MovingToObjectComponent>()
        .for_each(|obj| {
            let position = obj.component::<PositionComponent>().unwrap().point().clone();
            let target = obj.component::<MovingToObjectComponent>().unwrap().object_id().clone();
            let target = position_by_id.get(&target).unwrap();

            if target.distance_to_point(&position) > 500.0 {
                let mut desired_velocity = (target - &position).to_vector().normalize();
                let rotation = Vector::new(0.0, 1.0).angle(&desired_velocity);

                for other_car in &car_positions {
                    let vector_to_other_car = (other_car - &position).to_vector();
                    if vector_to_other_car.magnitude() < DELTA {
                        continue;
                    }

                    desired_velocity = (desired_velocity - vector_to_other_car.normalize() * 0.4 * (500.0 / vector_to_other_car.magnitude())).normalize();
                }

                obj.component_mut::<WalkingForce>().unwrap().set(desired_velocity);
                obj.component_mut::<RotationComponent>().unwrap().set(rotation);
            }
        })
}