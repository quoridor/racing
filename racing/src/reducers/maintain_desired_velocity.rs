use crate::components::desired_velocity::DesiredVelocityComponent;
use crate::components::eliminated_car::EliminatedCarComponent;
use crate::components::engine_force::EngineForceComponent;
use crate::components::steering_angle::SteeringAngleComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{Angle, DELTA, Vector};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

pub fn maintain_desired_velocity_reducer(world: &mut World, _: f64, _: &InputContext) {
    for car in world.objects_by_component_mut::<DesiredVelocityComponent>() {
        if car.contains_component::<EliminatedCarComponent>() {
            car.component_mut::<EngineForceComponent>().unwrap().set(0.0);
            continue;
        }

        let desired_velocity = car.component::<DesiredVelocityComponent>().unwrap().desired_velocity().clone();
        let actual_velocity = car.component::<VelocityComponent>().unwrap().velocity().clone();
        let steering_angle = car.component::<SteeringAngleComponent>().unwrap().angle().clone();

        if desired_velocity.magnitude() < DELTA {
            car.component_mut::<EngineForceComponent>().unwrap().set(0.0);
            continue;
        }

        if actual_velocity.magnitude() < DELTA {
            car.component_mut::<EngineForceComponent>().unwrap().set(1.0);
            continue;
        }

        let actual_velocity_perpendicular = Vector::new(
            -actual_velocity.normalize().y(),
            actual_velocity.normalize().x(),
        );

        // if negative, turn left. If positive, turn right.
        let dot = actual_velocity_perpendicular.dot(&desired_velocity);
        let angle_diff = desired_velocity.angle(&actual_velocity).degrees();
        let steering_diff = angle_diff * dot.signum();
        let steering_angle = Angle::from_degrees(steering_diff);

        car.component_mut::<SteeringAngleComponent>().unwrap().set_angle(steering_angle);
        car.component_mut::<EngineForceComponent>().unwrap().set(desired_velocity.magnitude().min(1.0));
    }
}