use crate::components::rotation::RotationComponent;
use crate::components::rotation_movement::RotationMovementComponent;
use crate::geometry::Angle;
use crate::input::InputContext;
use crate::world::World;

pub fn rotation_movement_reducer(world: &mut World, delta: f64, _: &InputContext) {
    world.objects_by_component_mut::<RotationMovementComponent>()
        .for_each(|obj| obj.component_mut::<RotationComponent>().unwrap().add(&Angle::from_degrees(delta * 0.1)))
}