use crate::components::collision_body::CollisionBodyComponent;
use crate::components::collision_force::CollisionForceComponent;
use crate::components::eliminated_car::EliminatedCarComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{DELTA, Vector};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::{World, WorldObject};

pub fn collisions_reducer(world: &mut World, delta: f64, _: &InputContext) {
    let mut collision_bodies: Vec<&mut WorldObject> = world.objects_by_component_mut::<CollisionBodyComponent>().collect();

    for body in &mut collision_bodies {
        body.component_mut::<CollisionForceComponent>().unwrap().set(Vector::zero());
    }

    for a in 0..collision_bodies.len() {
        let first_body_position = collision_bodies[a].component::<PositionComponent>().unwrap().point().clone();
        let first_body_rotation = collision_bodies[a].component::<RotationComponent>().unwrap().angle().clone();
        let first_body_velocity = collision_bodies[a].component::<VelocityComponent>().unwrap().velocity().clone();
        let first_body = &collision_bodies[a].component::<CollisionBodyComponent>().unwrap();

        let first_body_polygon = first_body.polygon()
            .translated_by(&first_body_position.to_vector())
            .rotated_by(&first_body_rotation);

        for b in 0..collision_bodies.len() {
            if a == b {
                continue;
            }

            let second_body_position = collision_bodies[b].component::<PositionComponent>().unwrap().point().clone();
            let second_body_rotation = collision_bodies[b].component::<RotationComponent>().unwrap().angle().clone();
            let second_body_velocity = collision_bodies[b].component::<VelocityComponent>().unwrap().velocity().clone();
            let second_body = &collision_bodies[b].component::<CollisionBodyComponent>().unwrap();

            let second_body_polygon = second_body.polygon()
                .translated_by(&second_body_position.to_vector())
                .rotated_by(&second_body_rotation);

            if let Some(intersection_point) = first_body_polygon.intersects(&second_body_polygon) {
                let (_, d, _) = second_body_polygon.closest_edge(intersection_point);
                let collision_vector = (&first_body_polygon.center() - &second_body_polygon.center()).to_vector().normalize() * d;

                collision_bodies[a].component_mut::<CollisionForceComponent>().unwrap().add(collision_vector.clone());
                collision_bodies[b].component_mut::<CollisionForceComponent>().unwrap().add(collision_vector * -1.0);
            }
        }
    }
}