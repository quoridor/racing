use crate::components::collision_force::CollisionForceComponent;
use crate::components::latitudinal_force::LatitudinalForceComponent;
use crate::components::longtitudinal_force::LongtitudinalForceComponent;
use crate::components::pedestrian_movement_resistance::PedestrianMovementResistanceComponent;
use crate::components::velocity::VelocityComponent;
use crate::components::walking_force::WalkingForce;
use crate::geometry::{DELTA, Vector};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

pub fn velocity_reducer(world: &mut World, delta: f64, _: &InputContext) {
    world.objects_by_component_mut::<VelocityComponent>()
        .for_each(|obj| {
            let longtitudinal_force = obj.component::<LongtitudinalForceComponent>().map(|v| v.force().clone()).unwrap_or(Vector::zero());
            let latitudinal_force = obj.component::<LatitudinalForceComponent>().map(|v| v.force().clone()).unwrap_or(Vector::zero());
            let collision_force = obj.component::<CollisionForceComponent>().map(|v| v.force().clone()).unwrap_or(Vector::zero());

            let pedestrian_movement_resistance_force = obj.component::<PedestrianMovementResistanceComponent>().map(|v| v.force().clone()).unwrap_or(Vector::zero());
            let walking_force = obj.component::<WalkingForce>().map(|v| v.force().clone()).unwrap_or(Vector::zero());

            let combined_force = longtitudinal_force + latitudinal_force + collision_force + pedestrian_movement_resistance_force + walking_force;

            obj.component_mut::<VelocityComponent>().unwrap().add(&(combined_force * delta * 0.001));
        })
}