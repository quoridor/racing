use crate::components::position::PositionComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{DELTA, Vector};
use crate::input::InputContext;
use crate::world::World;

pub fn apply_velocity_reducer(world: &mut World, delta: f64, _: &InputContext) {
    world.objects_by_component_mut::<VelocityComponent>()
        .for_each(|obj| {
            let velocity = obj.component::<VelocityComponent>().unwrap().velocity().clone();
            let velocity = if velocity.magnitude() < DELTA || velocity.is_nan() {
                Vector::zero()
            } else {
                velocity
            };
            obj.component_mut::<PositionComponent>().unwrap().add_vector(&(&velocity * delta));
        });
}