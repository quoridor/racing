use std::collections::HashMap;
use crate::components::car::CarComponent;
use crate::components::controllable_car::ControllableCarComponent;
use crate::components::desired_velocity::DesiredVelocityComponent;
use crate::components::eliminated_car::EliminatedCarComponent;
use crate::components::id::{Id, IdComponent};
use crate::components::police_base::PoliceBaseComponent;
use crate::components::police_car::PoliceCarComponent;
use crate::components::position::PositionComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{Point, Vector};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

const BASE_PROXIMITY: f64 = 150.0;
const TRIGGER_VELOCITY: f64 = 0.9;
const TRIGGER_PROXIMITY: f64 = 400.0;
const MAX_PURSUIT_DISTANCE: f64 = 1000.0;
const CAPTURE_DISTANCE: f64 = 150.0;

pub fn police_car_reducer(world: &mut World, _: f64, _: &InputContext) {
    let mut police_bases: Vec<(Point, Vec<Id>)> = Vec::new();
    for police_base in world.objects_by_component::<PoliceBaseComponent>() {
        let base_position = police_base.component::<PositionComponent>().unwrap().point().clone();
        police_bases.push((base_position, Vec::new()));
    }

    let mut car_positions = HashMap::new();
    let mut speeding_cars = Vec::new();
    for car in world.objects_by_component::<CarComponent>() {
        if car.component::<EliminatedCarComponent>().is_some() {
            continue;
        }

        let id = car.component::<IdComponent>().unwrap().id().clone();
        let position = car.component::<PositionComponent>().unwrap().point().clone();
        let velocity = car.component::<VelocityComponent>().unwrap().velocity().magnitude();

        car_positions.insert(id.clone(), position.clone());

        if velocity > TRIGGER_VELOCITY {
            speeding_cars.push((id, position));
        }
    }

    let mut captured_cars = Vec::new();

    for police_car in world.objects_by_component_mut::<PoliceCarComponent>() {
        let id = police_car.component::<IdComponent>().unwrap().id().clone();
        let police = police_car.component::<PoliceCarComponent>().unwrap().clone();
        let position = police_car.component::<PositionComponent>().unwrap()
            .point()
            .clone();

        if police.following_car().is_none() {
            let mut closest_base: Option<Point> = None;
            let mut at_base = false;

            for i in 0..police_bases.len() {
                let base_position = &police_bases[i].0;
                let base_to_car = base_position.distance_to_point(&position);

                if police_bases[i].1.is_empty() {
                    if closest_base.is_none() || base_to_car < closest_base.as_ref().unwrap().distance_to_point(&position) {
                        closest_base = Some(base_position.clone());
                    }

                    if base_to_car < BASE_PROXIMITY {
                        police_bases.get_mut(i).unwrap().1.push(id.clone());
                        at_base = true;
                        break;
                    }
                }
            }

            if !at_base && closest_base.is_some() {
                let distance_to_closest = position.distance_to_point(&closest_base.as_ref().unwrap()) / 1000.0;
                let desired = Vector::from_points(&position, &closest_base.unwrap()).normalize() * distance_to_closest;
                police_car.component_mut::<DesiredVelocityComponent>().unwrap().set(desired);
            } else {
                police_car.component_mut::<DesiredVelocityComponent>().unwrap().set(Vector::zero());

                let closest_speeding = speeding_cars.iter()
                    .reduce(|a, b| {
                        let a_dist = a.1.distance_to_point(&position);
                        let b_dist = b.1.distance_to_point(&position);

                        if a_dist < b_dist {
                            a
                        } else {
                            b
                        }
                    });
                if closest_speeding.is_some() && closest_speeding.as_ref().unwrap().1.distance_to_point(&position) < TRIGGER_PROXIMITY {
                    police_car.component_mut::<PoliceCarComponent>().unwrap().set_following_car(closest_speeding.as_ref().unwrap().0.clone());
                }
            }
        } else {
            let following_car_position = match car_positions.get(police.following_car().unwrap()) {
                Some(v) => v,
                None => {
                    police_car.component_mut::<PoliceCarComponent>().unwrap().clear_following_car();
                    continue;
                }
            };

            let distance_to_car = following_car_position.distance_to_point(&position);
            if distance_to_car > MAX_PURSUIT_DISTANCE {
                police_car.component_mut::<PoliceCarComponent>().unwrap().clear_following_car();
            } else if distance_to_car < CAPTURE_DISTANCE {
                captured_cars.push(police.following_car().unwrap().clone());
            } else {
                let desired = Vector::from_points(&position, &following_car_position).normalize();
                police_car.component_mut::<DesiredVelocityComponent>().unwrap().set(desired);
            }
        }
    }

    for car in world.objects_by_component_mut::<CarComponent>() {
        let id = match car.component::<IdComponent>() {
            Some(id) => id.id().clone(),
            None => continue,
        };

        if captured_cars.contains(&id) {
            car.add_component(EliminatedCarComponent::new());
        }
    }
}