use crate::components::latitudinal_force::LatitudinalForceComponent;
use crate::components::rotation::RotationComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{DELTA, Vector};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

const WHEEL_FRICTION_FACTOR: f64 = 4.0;

pub fn latitudinal_force_reducer(world: &mut World, _: f64, _: &InputContext) {
    world.objects_by_component_mut::<LatitudinalForceComponent>()
        .for_each(|obj| {
            let rotation = obj.component::<RotationComponent>().unwrap().angle().unit_vector();
            let velocity = obj.component::<VelocityComponent>().unwrap().velocity();
            if velocity.magnitude() < DELTA || velocity.is_nan() {
                obj.component_mut::<LatitudinalForceComponent>().unwrap().set(Vector::zero());
                return;
            }

            let angle_between_rotation_and_velocity = rotation.angle(&velocity);
            let friction = angle_between_rotation_and_velocity.radians().sin() * velocity.magnitude() * WHEEL_FRICTION_FACTOR;

            let rotation_perpendicular_1 = Vector::new(-rotation.y(), rotation.x());
            let rotation_perpendicular_2 = Vector::new(rotation.y(), -rotation.x());

            let rotation_perpendicular = if rotation_perpendicular_1.dot(velocity) < 0.0 {
                rotation_perpendicular_1
            } else {
                rotation_perpendicular_2
            };
            let latitudinal_force = &rotation_perpendicular / rotation_perpendicular.magnitude() * friction;
            let latitudinal_force = if latitudinal_force.magnitude() < DELTA || latitudinal_force.is_nan() {
                Vector::zero()
            } else {
                latitudinal_force
            };

            obj.component_mut::<LatitudinalForceComponent>().unwrap().set(latitudinal_force);
        })
}