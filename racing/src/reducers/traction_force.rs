use crate::components::engine_force::EngineForceComponent;
use crate::components::max_engine_power::MaxEnginePowerComponent;
use crate::components::rotation::RotationComponent;
use crate::components::traction_force::TractionForceComponent;
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

pub fn traction_force_reducer(world: &mut World, _: f64, _: &InputContext) {
    world.objects_by_component_mut::<TractionForceComponent>()
        .for_each(|obj| {
            let engine_force = obj.component::<EngineForceComponent>().unwrap().force();
            let max_engine_power = obj.component::<MaxEnginePowerComponent>().unwrap().max_power();
            let rotation = obj.component::<RotationComponent>().unwrap().angle().unit_vector();
            obj.component_mut::<TractionForceComponent>().unwrap().set(rotation * engine_force.min(1.0).max(-0.35) * 2.0 * max_engine_power);
        })
}