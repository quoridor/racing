use crate::components::drag_force::DragForceComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::DELTA;
use crate::input::InputContext;
use crate::world::World;

const C_DRAG: f64 = 1.0;

pub fn drag_force_reducer(world: &mut World, _: f64, _: &InputContext) {
    world.objects_by_component_mut::<DragForceComponent>()
        .for_each(|obj| {
            let velocity = obj.component::<VelocityComponent>().unwrap().velocity().clone();
            if velocity.magnitude() < DELTA || velocity.is_nan() {
                return;
            }

            let drag_force = -C_DRAG * &velocity * velocity.magnitude();
            obj.component_mut::<DragForceComponent>().unwrap().set(drag_force);
        })
}