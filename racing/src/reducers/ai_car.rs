use crate::components::ai_car::AiCarComponent;
use crate::components::car::CarComponent;
use crate::components::desired_velocity::DesiredVelocityComponent;
use crate::components::engine_force::EngineForceComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::components::steering_angle::SteeringAngleComponent;
use crate::components::track::TrackComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{Angle, DELTA, Point, Segment, Vector};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

pub fn ai_car_reducer(world: &mut World, delta: f64, _: &InputContext) {
    let track = world.objects_by_component::<TrackComponent>().next().unwrap()
        .component::<TrackComponent>()
        .unwrap()
        .route()
        .clone();

    let car_positions: Vec<Point> = world.objects_by_component::<CarComponent>()
        .map(|v| v.component::<PositionComponent>().unwrap().point().clone())
        .collect();

    for ai_car in world.objects_by_component_mut::<AiCarComponent>() {
        let position = ai_car.component::<PositionComponent>().unwrap().point().clone();
        let velocity = ai_car.component::<VelocityComponent>().unwrap().velocity().magnitude();
        let ai_car_component = ai_car.component::<AiCarComponent>().unwrap();

        let next_track_point_index = ai_car_component.next_track_point();
        let prev_track_point_index = if next_track_point_index == 0 {
            track.len() - 1
        } else {
            next_track_point_index as usize - 1
        };
        let next_track_point = &track[next_track_point_index as usize];
        let current_segment = Segment::new((track[prev_track_point_index].clone(), next_track_point.clone()));

        if position.distance_to_point(&next_track_point) < 500.0 * velocity {
            ai_car.component_mut::<AiCarComponent>().unwrap().set_next_track_point((next_track_point_index + 1) % (track.len() as u32));
        }

        let mut desired_velocity = (next_track_point - &position).to_vector().normalize();

        for other_car in &car_positions {
            let vector_to_other_car = (other_car - &position).to_vector();
            if vector_to_other_car.magnitude() < DELTA {
                continue;
            }

            if vector_to_other_car.magnitude() <= 500.0 {
                desired_velocity = (desired_velocity - vector_to_other_car.normalize() * 0.1 * (500.0 - vector_to_other_car.magnitude()) / 100.0).normalize();
            }
        }

        let (dist, closest_segment_point) = position.distance_to_segment(&current_segment);
        desired_velocity = (desired_velocity + (closest_segment_point - &position).to_vector().normalize() * 0.001 * dist).normalize();

        ai_car.component_mut::<DesiredVelocityComponent>().unwrap().set(desired_velocity);

    }
}
