use crate::components::lap_counter::LapCounterComponent;
use crate::components::position::PositionComponent;
use crate::components::track::TrackComponent;
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

pub fn update_lap_counter_reducer(world: &mut World, _delta: f64, _input: &InputContext) {
    let track = world.objects_by_component::<TrackComponent>().next().unwrap()
        .component::<TrackComponent>()
        .unwrap()
        .route()
        .clone();

    for counter in world.objects_by_component_mut::<LapCounterComponent>() {
        let position = counter.component::<PositionComponent>().unwrap().point().clone();
        let lap_counter = counter.component_mut::<LapCounterComponent>().unwrap();

        let next_track_point_index = lap_counter.next_checkpoint();
        let next_track_point = &track[next_track_point_index as usize];

        if position.distance_to_point(&next_track_point) < 500.0 {
            if next_track_point_index == 1 {
                lap_counter.set_total_laps(lap_counter.total_laps() + 1);
            }

            let mut new_track_point = (next_track_point_index + 1) % track.len() as u32;

            lap_counter.set_next_checkpoint(new_track_point);
        }
    }
}