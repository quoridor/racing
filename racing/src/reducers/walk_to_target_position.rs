use rand::prelude::IteratorRandom;
use crate::components::car::CarComponent;
use crate::components::moving_to_position::MovingToPositionComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::components::walking_force::WalkingForce;
use crate::geometry::{DELTA, Point, Vector};
use crate::input::InputContext;
use crate::world::World;

pub fn walk_to_target_position_reducer(world: &mut World, _: f64, _: &InputContext) {
    let car_positions: Vec<Point> = world.objects_by_component::<CarComponent>()
        .map(|v| v.component::<PositionComponent>().unwrap().point().clone())
        .collect();

    world.objects_by_component_mut::<MovingToPositionComponent>()
        .for_each(|obj| {
            let position = obj.component::<PositionComponent>().unwrap().point().clone();
            let target = obj.component::<MovingToPositionComponent>().unwrap().target_position().clone();

            if target.distance_to_point(&position) <= 100.0 {
                let target_x = (-100..8000).choose(&mut rand::thread_rng()).unwrap() as f64;
                let target_y = (-100..8000).choose(&mut rand::thread_rng()).unwrap() as f64;
                let target_position = Point::new(target_x, target_y);
                obj.component_mut::<MovingToPositionComponent>().unwrap().set(target_position);
                return;
            }

            let mut desired_velocity = (target - &position).to_vector().normalize();
            let rotation = Vector::new(0.0, 1.0).angle(&desired_velocity);

            for other_car in &car_positions {
                let vector_to_other_car = (other_car - &position).to_vector();
                if vector_to_other_car.magnitude() < DELTA {
                    continue;
                }

                desired_velocity = (desired_velocity - vector_to_other_car.normalize() * 0.4 * (500.0 / vector_to_other_car.magnitude())).normalize();
            }

            obj.component_mut::<WalkingForce>().unwrap().set(desired_velocity);
            obj.component_mut::<RotationComponent>().unwrap().set(rotation);
        })
}