use crate::components::controllable_car::ControllableCarComponent;
use crate::components::eliminated_car::EliminatedCarComponent;
use crate::components::id::IdComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::DELTA;
use crate::input::InputContext;
use crate::world::World;

pub fn eliminated_cars_reducer(world: &mut World, _delta: f64, _input: &InputContext) {
    let mut cars_to_remove = Vec::new();

    for car in world.objects_by_component::<EliminatedCarComponent>() {
        let id = car.component::<IdComponent>().unwrap().id().clone();
        let velocity = car.component::<VelocityComponent>().unwrap().velocity().magnitude();

        if velocity < 0.01 && car.component::<ControllableCarComponent>().is_none() {
            cars_to_remove.push(id);
            continue;
        }
    }

    world.remove_objects(&cars_to_remove);
}