use crate::components::rotation::RotationComponent;
use crate::components::steering_angle::SteeringAngleComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::{Angle, DELTA};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

const DISTANCE_BETWEEN_WHEELS: f64 = 1.6;

pub fn apply_angular_velocity_reducer(world: &mut World, delta: f64, _: &InputContext) {
    world.objects_by_component_mut::<SteeringAngleComponent>()
        .for_each(|obj| {
            let steering_angle = obj.component::<SteeringAngleComponent>().unwrap().angle();
            let rotation = obj.component::<RotationComponent>().unwrap().angle().unit_vector();
            let velocity = obj.component::<VelocityComponent>().unwrap().velocity();
            let velocity_magnitude = velocity.magnitude();

            if velocity_magnitude < DELTA || velocity_magnitude.is_nan() {
                return;
            }

            let direction = velocity.dot(&rotation).signum();

            let turning_radius = DISTANCE_BETWEEN_WHEELS / steering_angle.radians().sin();
            let angular_velocity = direction * velocity_magnitude/turning_radius;
            obj.component_mut::<RotationComponent>().unwrap().add(&Angle::from_degrees(angular_velocity * delta));
        });
}