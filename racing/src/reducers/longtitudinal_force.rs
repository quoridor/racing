use crate::components::drag_force::DragForceComponent;
use crate::components::longtitudinal_force::LongtitudinalForceComponent;
use crate::components::rolling_resistance::RollingResistanceComponent;
use crate::components::traction_force::TractionForceComponent;
use crate::geometry::{DELTA, Vector};
use crate::input::InputContext;
use crate::utils::console_log;
use crate::world::World;

pub fn longtitudinal_force_reducer(world: &mut World, _: f64, _: &InputContext) {
    world.objects_by_component_mut::<LongtitudinalForceComponent>()
        .for_each(|obj| {
            let traction_force = obj.component::<TractionForceComponent>().unwrap().force().clone();
            let drag_force = obj.component::<DragForceComponent>().unwrap().force().clone();
            let rolling_resistance_force = obj.component::<RollingResistanceComponent>().unwrap().force().clone();

            let longtitudinal_force = traction_force + drag_force + rolling_resistance_force;
            let longtitudinal_force = if longtitudinal_force.magnitude() < DELTA || longtitudinal_force.is_nan() {
                Vector::zero()
            } else {
                longtitudinal_force
            };

            obj.component_mut::<LongtitudinalForceComponent>().unwrap().set(longtitudinal_force);
        })
}