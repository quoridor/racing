use std::collections::{HashMap, HashSet};
use crate::components::position::PositionComponent;
use crate::components::rolling_resistance::RollingResistanceComponent;
use crate::components::track::TrackComponent;
use crate::components::velocity::VelocityComponent;
use crate::geometry::Point;
use crate::input::InputContext;
use crate::world::World;

const C_ROLLING_RESISTANCE: f64 = 1.0;

pub fn rolling_resistance_reducer(world: &mut World, _: f64, _: &InputContext) {
    let positions: Vec<Point> = world.objects_by_component::<RollingResistanceComponent>()
        .map(|obj| obj.component::<PositionComponent>().unwrap().point().clone())
        .collect();

    let positions: HashSet<(u32, u32)> = {
        let track = world.objects_by_component::<TrackComponent>().next().unwrap()
            .component::<TrackComponent>()
            .unwrap()
            .tiles();

        positions.iter()
            .map(|pos| (pos.x().round() as u32, pos.y().round() as u32))
            .filter(|(x_rounded, y_rounded)| {
                track.iter()
                    .find(|tile|
                        tile.position().x() < *x_rounded as f64 &&
                            tile.position().y() < *y_rounded as f64 &&
                            tile.position().x() + tile.size().width() >= *x_rounded as f64 &&
                            tile.position().y() + tile.size().height() >= *y_rounded as f64)
                    .is_some()
            })
            .collect()
    };

    world.objects_by_component_mut::<RollingResistanceComponent>()
        .for_each(|obj| {
            let velocity = obj.component::<VelocityComponent>().unwrap().velocity().clone();
            if velocity.is_nan() {
                return;
            }

            let position = obj.component::<PositionComponent>().unwrap().point().clone();

            let rolling_resistance = -C_ROLLING_RESISTANCE * velocity * if positions.contains(&(position.x().round() as u32, position.y().round() as u32)) {
                1.0
            } else {
                3.0
            };
            obj.component_mut::<RollingResistanceComponent>().unwrap().set(rolling_resistance);
        })
}