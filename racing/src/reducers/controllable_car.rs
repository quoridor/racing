use crate::components::car::{CarComponent};
use crate::components::controllable_car::ControllableCarComponent;
use crate::components::eliminated_car::EliminatedCarComponent;
use crate::components::engine_force::EngineForceComponent;
use crate::components::position::PositionComponent;
use crate::components::rotation::RotationComponent;
use crate::components::steering_angle::SteeringAngleComponent;
use crate::geometry::{Vector, Angle};
use crate::input::InputContext;
use crate::world::World;

pub fn controllable_car_reducer(world: &mut World, delta: f64, input_context: &InputContext) {
    let engine_force = if input_context.is_input_forward() {
        1.0
    } else if input_context.is_input_backward() {
        -1.0
    } else {
        0.0
    };

    let angle = if input_context.is_input_turn_left() {
        Some(Angle::from_degrees(-1.0))
    } else if input_context.is_input_turn_right() {
        Some(Angle::from_degrees(1.0))
    } else {
        None
    }.map(|v| v * delta * 0.1);

    world.objects_by_component_mut::<ControllableCarComponent>()
        .for_each(|car| {
            if car.contains_component::<EliminatedCarComponent>() {
                car.component_mut::<EngineForceComponent>().unwrap().set(0.0);
                return;
            }

            car.component_mut::<EngineForceComponent>().unwrap().set(engine_force);

            let steering_angle = car.component::<SteeringAngleComponent>().unwrap().angle().clone();
            let angle = match &angle {
                Some(angle) => (&steering_angle + angle)
                    .min(Angle::from_degrees(30.0))
                    .max(Angle::from_degrees(-30.0)),
                None => {
                    let steer_to = &steering_angle + Angle::from_degrees(steering_angle.signum() * -1.0 * delta * 0.1);
                    if steering_angle.signum() != steer_to.signum() {
                        Angle::zero()
                    } else {
                        steer_to
                    }
                }
            };

            let angle = if angle.is_close_to_zero() {
                Angle::zero()
            } else {
                angle
            };

            car.component_mut::<SteeringAngleComponent>().unwrap().set_angle(angle);
        });
}